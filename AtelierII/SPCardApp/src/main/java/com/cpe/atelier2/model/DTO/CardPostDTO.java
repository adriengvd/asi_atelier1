package com.cpe.atelier2.model.DTO;

public class CardPostDTO {

    private int type;
    private boolean toSell;
    private int owner;

	public CardPostDTO() {}

	public CardPostDTO(int type, boolean toSell, int owner) {
		this.type = type;
        this.toSell = toSell;
        this.owner = owner;
	}

    public int getType() {
        return this.type;
    } 

    public boolean isSellable() {
        return this.toSell;
    }

    public int getOwner() {
        return this.owner;
    }
    
}
