package com.cpe.atelier2.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cpe.atelier2.model.CardType;
import com.cpe.atelier2.service.CardTypeService;

@RestController
@RequestMapping(path = "${apiPrefix}")
public class CardTypeRestController {

	@Autowired
	CardTypeService cardTypeService;
	
	@RequestMapping(method=RequestMethod.POST,value="/card-type")
	public ResponseEntity<String> addCard(@RequestBody CardType cardType) {

		try {
			cardTypeService.addCardType(cardType);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
		}

		return ResponseEntity.status(HttpStatus.CREATED).body(null);
	}

	@RequestMapping(method=RequestMethod.GET, value="/cardType/init")
	public void initCardType() {
		cardTypeService.initCardTypes();		
	}
	
}
