package com.cpe.atelier2.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.TextCodec;

import java.util.Calendar;
import java.util.Date;

import com.cpe.atelier2.Exception.NotFoundException;
import com.cpe.atelier2.Exception.NotValidPasswordException;
import com.cpe.atelier2.model.User;
import com.cpe.atelier2.repository.UserRepository;

@Service
public class UserService {

	private static int JWTHourExpiration = 3;

	@Autowired
	UserRepository userRepo;

	@Autowired
	CardService cardService;

	public void addUser(User user) throws IllegalArgumentException {
		this.setNewUserBalance(user);
		this.encryptPassword(user);
		userRepo.save(user);
		cardService.initUserCards(user);
	}
	
	public User getUser(int id) {
		return userRepo.findById(id).orElse(null);
	}
	
	public User getUser(String username) {
		return userRepo.findByUsername(username).orElse(null);
	}

	public String loginUser(String username, String loginPassword) throws NotFoundException, NotValidPasswordException {
		User user = this.getUser(username);

		if (user == null) {
			throw new NotFoundException();
		}

		if (! this.doesPasswordsMatch(user, loginPassword)) {
			throw new NotValidPasswordException();
		}

		String token = this.generateJWTToken(user);

		return token;
	}

	private boolean doesPasswordsMatch(User user, String loginPassword) {
		PasswordEncoder encoder = new BCryptPasswordEncoder();
		return encoder.matches(loginPassword, user.getPassword());
	}

	private String generateJWTToken(User user) {

		Date issuedAt = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(issuedAt);
		c.add(Calendar.HOUR, JWTHourExpiration);
		Date Expiration = c.getTime();

		return Jwts.builder()
				.setIssuer("SpCardApp")
				.setSubject(user.getId() + "")
				.claim("username", user.getUsername())
				.claim("userId", user.getId())
				.setIssuedAt(issuedAt)
				.setExpiration(Expiration)
				.signWith(
					SignatureAlgorithm.HS256,
					TextCodec.BASE64.decode("TVlTVVBFUlNFQ1JFVEpXVEtFWQ==")
				)
				.compact();
	}

	public void updateBalance(User user, float newBalance) {
		user.setBalance(newBalance);
	}

	private void setNewUserBalance(User user) {
		float balance = 5000f;
		user.setBalance(balance);
	}

	private void encryptPassword(User user) {
		PasswordEncoder encoder = new BCryptPasswordEncoder();
		String password = user.getPassword();
		user.setPassword(encoder.encode(password));
	}


}
