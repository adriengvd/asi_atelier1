package com.cpe.atelier2.model.DTO;

import java.util.Date;

public class TransactionDTO {

	private String buyer;
	private String seller;
	private int card;
	private float price;
	private Date date;
	
	public TransactionDTO() {}

	public TransactionDTO(String buyer, String seller, int card, float price, Date date) {
		this.buyer = buyer;
		this.seller = seller;
		this.card = card;
		this.price = price;
		this.date = date;
	}
	
	public String getBuyer() {
		return this.buyer;
	}

	public void setBuyer(String buyer) {
		this.buyer = buyer;
	}
	
	public String getSeller() {
		return this.seller;
	}

	public void setSeller(String seller) {
		this.seller = seller;
	}
	
	public int getCard() {
		return this.card;
	}
	
	public void setCard(int card) {
		this.card = card;
	}
	
	public float getPrice() {
		return this.price;
	}

	public void setPrice(float price) {
		this.price = price;
	}
	
	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

}
