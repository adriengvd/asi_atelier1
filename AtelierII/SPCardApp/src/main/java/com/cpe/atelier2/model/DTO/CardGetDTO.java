package com.cpe.atelier2.model.DTO;

import com.cpe.atelier2.model.CardType;

public class CardGetDTO {

    private CardType type;
    private boolean toSell;
    private int owner;
    private int id;

	public CardGetDTO() {}

	public CardGetDTO(CardType type, boolean toSell, int owner, int id) {
		this.type = type;
        this.toSell = toSell;
        this.owner = owner;
        this.id = id;
	}

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public CardType getType() {
        return this.type;
    }

    public void setType(CardType type) {
        this.type = type;
    }
    
    public boolean getToSell() {
        return this.toSell;
    }

    public void setToSell(boolean toSell) {
        this.toSell = toSell;
    }

    public int getOwner() {
        return this.owner;
    }

    public void setOwner(int owner) {
        this.owner = owner;
    }
}
