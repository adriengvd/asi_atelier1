package com.cpe.atelier2.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;

@Entity
public class Card {
	@Id
	@GeneratedValue
    private int id;

    @ManyToOne
    @JoinColumn(name="type_id", nullable=false)
    private CardType type;
    private boolean toSell;

    @ManyToOne
    @JoinColumn(name="owner_id", nullable=false)
    private User owner;

    public Card() {}

	public Card(CardType cardType, User owner) {
		this.type = cardType;
        this.toSell = false;
        this.owner = owner;
	}

    public int getId() {
        return this.id;
    }

    public CardType getType() {
        return this.type;
    }

    public void setType(CardType type) {
        this.type = type;
    }

    public Boolean getToSell() {
        return this.toSell;
    }

    public void setToSell(boolean toSell) {
        this.toSell = toSell;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public String toString() {
        String response;
        response = "isSellable: " + this.getToSell();
        if (this.owner != null) {
            response += ", Owner: " + this.owner.getUsername();
        } 
        if (this.type != null) {
            response += ", CardType: " + this.type.getId();
        } 

        return response;
    }
}
