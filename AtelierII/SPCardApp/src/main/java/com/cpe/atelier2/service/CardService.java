package com.cpe.atelier2.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

import com.cpe.atelier2.Exception.NotCardOwnerException;
import com.cpe.atelier2.Exception.NotEnoughMoneyException;
import com.cpe.atelier2.Exception.NotFoundException;
import com.cpe.atelier2.Exception.NotSellableException;
import com.cpe.atelier2.Exception.SameOwnerException;
import com.cpe.atelier2.model.Card;
import com.cpe.atelier2.model.CardType;
import com.cpe.atelier2.model.Transaction;
import com.cpe.atelier2.model.User;
import com.cpe.atelier2.repository.CardRepository;

@Service
public class CardService {
	@Autowired
	CardRepository cardRepo;
	@Autowired
	CardTypeService cardTypeService;
	@Autowired
	TransactionService transactionService;
	@Autowired
	UserService userService;

	public void addCard(Card card) throws IllegalArgumentException {
		cardRepo.save(card);
	}

	public void buyCard(int cardId, int buyerId) throws IllegalArgumentException, NotFoundException, NotEnoughMoneyException, NotSellableException, SameOwnerException {
		Card card = cardRepo.findById(cardId).orElse(null);
		User user = userService.getUser(buyerId);
		if (card == null || user == null) {
			throw new NotFoundException();
		}
		if (!card.getToSell()) {
			throw new NotSellableException();
		}
		if (card.getOwner().getId() == buyerId) {
			throw new SameOwnerException();
		}
		if (user.getBalance() < card.getType().getPrice()) {
			throw new NotEnoughMoneyException();
		}

		userService.updateBalance(user, user.getBalance() - card.getType().getPrice());

		Transaction transaction = new Transaction(new Date(), card, user, card.getOwner(), card.getType().getPrice());
		transactionService.addTransaction(transaction);

		card.setOwner(user);
		card.setToSell(false);

		cardRepo.save(card);
	}

	public void sellCard(int cardId, int connectedUserId) throws NotCardOwnerException {
		Card card = cardRepo.findById(cardId).orElse(null);

		if (card.getOwner().getId() != connectedUserId) {
			throw new NotCardOwnerException();
		}

		card.setToSell(true);
		cardRepo.save(card);

	}
	
	public Card getCard(int id) {
		return cardRepo.findById(id).orElse(null);
	}

	public List<Card> getCards() {
		return (List<Card>) cardRepo.findAll();
	}

	public List<Card> getUserCards(int userId) {
		User user = userService.getUser(userId);
		return (List<Card>) cardRepo.findByOwner(user);
	}

	public List<Card> getBuyableCards(int userId) {
		User user = userService.getUser(userId);
		return (List<Card>) cardRepo.findByOwnerNotAndToSell(user, true);
	}

	public List<Card> getSellableCards(int userId) {
		User user = userService.getUser(userId);
		return (List<Card>) cardRepo.findByOwnerAndToSell(user, false);
	}

	public void initUserCards(User user) {
		List<CardType> lastFiveCardTypes = cardTypeService.getLastFiveCardTypes();
		Card card;
		for (CardType cardType : lastFiveCardTypes) {
			card = new Card(cardType, user);
			cardRepo.save(card);
		}
	}


}
