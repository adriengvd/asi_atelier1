package com.cpe.atelier2.model.DTO;

public class UserDTO {

	private String name;
	private String surname;
	private String username;
	private String password;
	private String rePassword;

	public UserDTO() {}

	public UserDTO(String name, String surname, String password, String rePassword, String username) {
		this.name = name;
		this.surname = surname;
		this.password = password;
		this.rePassword = rePassword;
		this.username = username;
	}
	
	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

	public String getPassword() {
		return password;
	}

	public String getRePassword() {
		return rePassword;
	}

	public String getUsername() {
		return this.username;
	}
}
