package com.cpe.atelier2.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import com.cpe.atelier2.mapper.TransactionMapper;
import com.cpe.atelier2.model.Transaction;
import com.cpe.atelier2.model.DTO.TransactionDTO;
import com.cpe.atelier2.service.TransactionService;

@RestController
@RequestMapping(path = "${apiPrefix}")
public class TransactionRestController {

	@Autowired
	TransactionService transactionService;

	@Autowired
	TransactionMapper transactionMapper;
	
	@RequestMapping(method=RequestMethod.GET,value="/transaction/{id}")
	public TransactionDTO getTransaction(@PathVariable String id) {
		Transaction transaction  = transactionService.getTransaction(Integer.valueOf(id));
		return transactionMapper.convertToTransactionDTO(transaction);
	}
	
	@RequestMapping(method=RequestMethod.GET,value="/transactions")
	public List<TransactionDTO> getTransactions() {
		List<Transaction> transactions  = transactionService.getTransactions();
		return transactionMapper.convertToListOfTransactionDTO(transactions);
	}

}
