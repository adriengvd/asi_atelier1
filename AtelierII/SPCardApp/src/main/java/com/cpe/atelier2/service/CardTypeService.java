package com.cpe.atelier2.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import com.cpe.atelier2.model.CardType;
import com.cpe.atelier2.repository.CardTypeRepository;

@Service
public class CardTypeService {
	@Autowired
	CardTypeRepository cardTypeRepo;

	public void addCardType(CardType cardType) throws IllegalArgumentException {
		cardTypeRepo.save(cardType);
	}
	
	public CardType getCardType(int id) {
		return cardTypeRepo.findById(id).orElse(null);
	}

	public List<CardType> getLastFiveCardTypes() {
		return cardTypeRepo.findLastFiveCardTypes();
	}

	public void initCardTypes() {
		List<CardType> allNewCardTypes 	= new ArrayList<CardType>(){};

		CardType isInit = cardTypeRepo.findByName("Osselait").orElse(null);
		if (isInit != null) {
			return;
		}	

		CardType ct = new CardType("Salamèche", "Gigachad", "Pokemon", "Feu", "https://www.pokepedia.fr/images/thumb/e/ef/Bulbizarre-RFVF.png/250px-Bulbizarre-RFVF.png", "https://www.pokepedia.fr/images/thumb/e/ef/Bulbizarre-RFVF.png/250px-Bulbizarre-RFVF.png", 50f, 46f, 2f, 12f, 500f);
		allNewCardTypes.add(ct);
		ct = new CardType("Bulzibarre", "Ami de la nature", "Pokemon", "Herbe", "https://www.pokepedia.fr/images/thumb/8/89/Salam%C3%A8che-RFVF.png/250px-Salam%C3%A8che-RFVF.png", "https://www.pokepedia.fr/images/thumb/8/89/Salam%C3%A8che-RFVF.png/250px-Salam%C3%A8che-RFVF.png", 2f, 3f, 1f, 5f, 200f);
		allNewCardTypes.add(ct);
		ct = new CardType("Carapuce", "Poisson", "Pokemon", "Eau", "https://www.pokepedia.fr/images/thumb/c/cc/Carapuce-RFVF.png/250px-Carapuce-RFVF.png", "https://www.pokepedia.fr/images/thumb/c/cc/Carapuce-RFVF.png/250px-Carapuce-RFVF.png", 90f, 50f, 70f, 30f, 1500f);
		allNewCardTypes.add(ct);
		ct = new CardType("Osselait", "Edgelord", "Pokemon", "Sol", "https://www.pokepedia.fr/images/thumb/5/58/Osselait-RFVF.png/250px-Osselait-RFVF.png", "https://www.pokepedia.fr/images/thumb/5/58/Osselait-RFVF.png/250px-Osselait-RFVF.png", 36f, 45f, 39f, 76f, 1200f);
		allNewCardTypes.add(ct);
		ct = new CardType("Psykokwak", "Canard", "Pokemon", "Eau", "https://www.pokepedia.fr/images/thumb/4/44/Psykokwak-RFVF.png/250px-Psykokwak-RFVF.png", "https://www.pokepedia.fr/images/thumb/4/44/Psykokwak-RFVF.png/250px-Psykokwak-RFVF.png", 90f, 35f, 12f, 87f, 13000f);
		allNewCardTypes.add(ct);

		for (CardType cardType: allNewCardTypes) {
			cardTypeRepo.save(cardType);
		}
	
	}

}
