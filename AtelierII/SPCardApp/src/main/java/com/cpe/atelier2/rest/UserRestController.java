package com.cpe.atelier2.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import com.cpe.atelier2.Exception.NotFoundException;
import com.cpe.atelier2.Exception.NotValidPasswordException;
import com.cpe.atelier2.mapper.UserMapper;
import com.cpe.atelier2.model.User;
import com.cpe.atelier2.model.DTO.UserDTO;
import com.cpe.atelier2.model.DTO.UserLoginDTO;
import com.cpe.atelier2.service.UserService;

@RestController
@RequestMapping(path = "${apiPrefix}")
public class UserRestController {

	@Autowired
	UserService userService;

	@Autowired
	UserMapper userMapper;
	
	@RequestMapping(method=RequestMethod.POST,value="/user")
	public ResponseEntity<String> addUser(@RequestBody UserDTO userDTO) {

		if (userService.getUser(userDTO.getUsername()) != null) {
			return ResponseEntity.status(HttpStatus.CONFLICT).body(null);
		}

		if (! userDTO.getPassword().equals(userDTO.getRePassword())) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
		}

		try {
			userService.addUser(userMapper.convertFromUserDTO(userDTO));
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
		}

		return ResponseEntity.status(HttpStatus.CREATED).body(null);
	}
	
	@RequestMapping(method=RequestMethod.GET,value="/user/{id}")
	public User getUser(@PathVariable String id) {
		User user  = userService.getUser(Integer.valueOf(id));
		return user;
	}
	
	@RequestMapping(method=RequestMethod.POST,value="/user/login")
	public ResponseEntity<?> login(@RequestBody UserLoginDTO userLoginDTO, HttpServletResponse response) {
		String token;
		try {
			token = userService.loginUser(userLoginDTO.getUsername(), userLoginDTO.getPassword());
		} catch (NotFoundException e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		} catch (NotValidPasswordException e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}

		Cookie jwtCookie = this.createJWTCookie(token);
		Cookie userCookie = this.createUserInfoCookie(userLoginDTO.getUsername());
		response.addCookie(jwtCookie);
		response.addCookie(userCookie);
		return ResponseEntity.status(HttpStatus.OK).build();
	}

	@RequestMapping(method=RequestMethod.GET,value="/user/logout")
	public ResponseEntity<?> logout(HttpServletResponse response) {
		Cookie cookie = this.createJWTCookie(null);
		response.addCookie(cookie);
		return ResponseEntity.status(HttpStatus.OK).build();
	}

	private Cookie createJWTCookie(String token) {
		Cookie cookie = new Cookie("JWT", token);
		cookie.setHttpOnly(true);
		cookie.setSecure(false);
		cookie.setDomain("localhost");
		cookie.setPath("/");
		return cookie;
	}

	private Cookie createUserInfoCookie(String username) {
		User user = userService.getUser(username);
		
		String cookieInfo ="username:" + username + "&balance:" + Float.toString(user.getBalance()) + "&id:" + Integer.toString(user.getId());
		System.out.println(cookieInfo);

		Cookie cookie = new Cookie("userInfo", cookieInfo);
		cookie.setHttpOnly(false);
		cookie.setSecure(false);
		cookie.setDomain("localhost");
		cookie.setPath("/");
		return cookie;
	}


	
}
