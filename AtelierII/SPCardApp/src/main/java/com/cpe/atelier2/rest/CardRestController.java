package com.cpe.atelier2.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.impl.TextCodec;

import java.util.List;

import com.cpe.atelier2.Exception.NotCardOwnerException;
import com.cpe.atelier2.Exception.NotEnoughMoneyException;
import com.cpe.atelier2.Exception.NotFoundException;
import com.cpe.atelier2.Exception.NotSellableException;
import com.cpe.atelier2.Exception.SameOwnerException;
import com.cpe.atelier2.mapper.CardMapper;
import com.cpe.atelier2.model.Card;

import com.cpe.atelier2.model.DTO.CardGetDTO;
import com.cpe.atelier2.model.DTO.CardPostDTO;
import com.cpe.atelier2.service.CardService;
import com.cpe.atelier2.service.CardTypeService;
import com.cpe.atelier2.service.UserService;

@RestController
@RequestMapping(path = "${apiPrefix}")
public class CardRestController {

	@Autowired
	CardService cardService;
	@Autowired
	UserService userService;
	@Autowired
	CardTypeService cardTypeService;
	@Autowired
	CardMapper cardMapper;
	
	@RequestMapping(method=RequestMethod.POST,value="/card")
	public ResponseEntity<String> addCard(@RequestBody CardPostDTO cardDTO) {
		try {
			Card card = cardMapper.convertFromPostDTO(cardDTO);
			cardService.addCard(card);
		} catch (ResponseStatusException e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
		}
		 catch(IllegalArgumentException e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
		}

		return ResponseEntity.status(HttpStatus.CREATED).body(null);
	}
	
	@RequestMapping(method=RequestMethod.GET,value="/card/{id}")
	public CardGetDTO getCard(@PathVariable String id) {
		Card card = cardService.getCard(Integer.valueOf(id));
		return cardMapper.convertToGetCard(card);
	}
	
	@RequestMapping(method=RequestMethod.GET,value="/card/{id}/buy")
	public ResponseEntity<String> buyCard(@CookieValue(value="JWT") String jwt, @PathVariable String id) {

		String connectedUserId = Jwts.parser()
									.setSigningKey(TextCodec.BASE64.decode("TVlTVVBFUlNFQ1JFVEpXVEtFWQ=="))
									.parseClaimsJws(jwt)
									.getBody()
									.getSubject();	

		int buyer = Integer.valueOf(connectedUserId);

		try {
			cardService.buyCard(Integer.valueOf(id), buyer);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
		} catch (NotFoundException | NotEnoughMoneyException | NotSellableException | SameOwnerException e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
		}

		return ResponseEntity.status(HttpStatus.OK).body(null);
	}

	@RequestMapping(method=RequestMethod.GET,value="/card/{id}/sell")
	public ResponseEntity<String> sellCard(@CookieValue(value="JWT") String jwt, @PathVariable String id) {

		String connectedUserId = Jwts.parser()
									.setSigningKey(TextCodec.BASE64.decode("TVlTVVBFUlNFQ1JFVEpXVEtFWQ=="))
									.parseClaimsJws(jwt)
									.getBody()
									.getSubject();	
		
		try {
			cardService.sellCard(Integer.valueOf(id), Integer.valueOf(connectedUserId));
		} catch (NotCardOwnerException e) {
			return ResponseEntity.status(HttpStatus.FORBIDDEN).body(null);
		}
		return ResponseEntity.status(HttpStatus.OK).body(null);
	}

	@RequestMapping(method=RequestMethod.GET, value="/cards")
	public List<CardGetDTO> getCards() {
		List<Card> cards = cardService.getCards();
		return cardMapper.convertToListOfGetCards(cards);
	}

	@RequestMapping(method=RequestMethod.GET, value="/cards/user/{id}")
	public List<CardGetDTO> getUserCards(@PathVariable String id) {
		List<Card> cards = cardService.getUserCards(Integer.valueOf(id));
		return cardMapper.convertToListOfGetCards(cards);
	}

	@RequestMapping(method=RequestMethod.GET, value="/cards/buy")
	public List<CardGetDTO> getBuyableCards(@CookieValue(value="JWT") String jwt) {

		String connectedUserId = Jwts.parser()
		.setSigningKey(TextCodec.BASE64.decode("TVlTVVBFUlNFQ1JFVEpXVEtFWQ=="))
		.parseClaimsJws(jwt)
		.getBody()
		.getSubject();	

		List<Card> cards = cardService.getBuyableCards(Integer.valueOf(connectedUserId));
		return cardMapper.convertToListOfGetCards(cards);
	}

	@RequestMapping(method=RequestMethod.GET, value="/cards/sell")
	public List<CardGetDTO> getSellableCards(@CookieValue(value="JWT") String jwt) {

		String connectedUserId = Jwts.parser()
		.setSigningKey(TextCodec.BASE64.decode("TVlTVVBFUlNFQ1JFVEpXVEtFWQ=="))
		.parseClaimsJws(jwt)
		.getBody()
		.getSubject();	

		List<Card> cards = cardService.getSellableCards(Integer.valueOf(connectedUserId));
		return cardMapper.convertToListOfGetCards(cards);
	}





}
