package com.cpe.atelier2.mapper;

import java.util.ArrayList;
import java.util.List;

import com.cpe.atelier2.model.Card;
import com.cpe.atelier2.model.CardType;
import com.cpe.atelier2.model.User;
import com.cpe.atelier2.model.DTO.CardGetDTO;
import com.cpe.atelier2.model.DTO.CardPostDTO;
import com.cpe.atelier2.service.CardTypeService;
import com.cpe.atelier2.service.UserService;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import org.springframework.web.server.ResponseStatusException;

@Service
public class CardMapper {

    @Autowired
    UserService userService;

    @Autowired
    CardTypeService cardTypeService;

    public Card convertFromPostDTO(CardPostDTO cardDTO) throws ResponseStatusException {
        Card card = new Card();
		BeanUtils.copyProperties(cardDTO, card);

		User user = userService.getUser((Integer)cardDTO.getOwner());
		CardType cardType = cardTypeService.getCardType(cardDTO.getType());

		if (user == null || cardType == null) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No user with this id or no CardType with this id");
		}

		card.setOwner(user);
		card.setType(cardType);

        return card;
    }

    public CardGetDTO convertToGetCard(Card card) {
        CardGetDTO dto = new CardGetDTO();

        BeanUtils.copyProperties(card, dto);
        dto.setOwner(card.getOwner().getId());

        return dto;
    }

    public List<CardGetDTO> convertToListOfGetCards(List<Card> cards) {
        List<CardGetDTO> cardsDTO = new ArrayList<CardGetDTO>();

		for (Card card : cards) {
			cardsDTO.add(this.convertToGetCard(card));
		}
        return cardsDTO;
    }
}
