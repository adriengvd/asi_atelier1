package com.cpe.atelier2.filter;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

@Component
public class JWTTokenFilter extends OncePerRequestFilter {

    List<String> unsecuredAPIURLs = Arrays.asList("/api/user/login", "/api/user");
    List<String> unsecuredFrontURLs = Arrays.asList("/login", "/addUser");

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        String tokenJWT = null;

        Cookie[] cookies = request.getCookies();

        if (cookies != null) {
            tokenJWT = Arrays.stream(cookies)
                        .filter(c -> "JWT".equals(c.getName()))
                        .findFirst()
                        .map(cookie -> cookie.getValue())
                        .orElse(null);
        }
        // System.out.println("\n" + tokenJWT);
        if ((! request.getServletPath().startsWith("/api")) && tokenJWT == null) {
            response.sendRedirect("/login");
        }
        if (tokenJWT == null) response.setStatus(HttpStatus.UNAUTHORIZED.value());
        filterChain.doFilter(request, response);
    }

    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
        String path = request.getServletPath();
        String ressourcePattern = ".*(css|js|svg|jpg|jpeg|png|eot|otf|ttf|woff|woff2|ico)";
        boolean test1 = unsecuredAPIURLs.contains(request.getServletPath());
        boolean test2 = path.matches(ressourcePattern);
        // boolean test3 = unsecuredFrontURLs.contains(request.getServletPath());
        boolean test3 = false;
        test3 = unsecuredFrontURLs.stream().anyMatch(p -> path.startsWith(p));

        // System.out.println(request.getServletPath() + " = " + test1 + " " + test2 + " " + test3);
        return test1 || test2 || test3;
    }
}
