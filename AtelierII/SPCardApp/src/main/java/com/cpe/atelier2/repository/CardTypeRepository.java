package com.cpe.atelier2.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

import com.cpe.atelier2.model.CardType;

public interface CardTypeRepository extends CrudRepository<CardType, Integer> {

    public Optional<CardType> findByName(String name);
    
    @Query(value = "SELECT * from card_type order by id desc limit 5", nativeQuery = true)
    List<CardType> findLastFiveCardTypes();

}
