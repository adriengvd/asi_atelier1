package com.cpe.atelier2.repository;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

import com.cpe.atelier2.model.Transaction;

public interface TransactionRepository extends CrudRepository<Transaction, Integer> {

	public List<Transaction> findAll();

}
