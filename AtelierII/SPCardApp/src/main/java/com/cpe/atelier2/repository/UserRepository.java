package com.cpe.atelier2.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.cpe.atelier2.model.User;

public interface UserRepository extends CrudRepository<User, Integer> {

	public Optional<User> findByUsername(String username);
}
