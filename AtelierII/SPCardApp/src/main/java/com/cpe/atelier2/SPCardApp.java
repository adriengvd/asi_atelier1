package com.cpe.atelier2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;


@SpringBootApplication(exclude = {SecurityAutoConfiguration.class})
public class SPCardApp {	

	public static void main(String[] args) {
		SpringApplication.run(SPCardApp.class, args);
	}

}
