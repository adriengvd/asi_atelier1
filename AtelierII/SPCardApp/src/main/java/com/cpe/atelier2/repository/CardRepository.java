package com.cpe.atelier2.repository;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

import com.cpe.atelier2.model.Card;
import com.cpe.atelier2.model.User;

public interface CardRepository extends CrudRepository<Card, Integer> {

    List<Card> findByOwner(User owner);

    List<Card> findByOwnerNotAndToSell(User owner, boolean toSell);

    List<Card> findByOwnerAndToSell(User owner, boolean toSell);

}
