package com.cpe.atelier2.mapper;

import com.cpe.atelier2.model.User;
import com.cpe.atelier2.model.DTO.UserDTO;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

@Service
public class UserMapper {
    
    public User convertFromUserDTO(UserDTO userDTO) {
        User user = new User();
		BeanUtils.copyProperties(userDTO, user);

        return user;
    }
}
