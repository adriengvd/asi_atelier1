package com.cpe.atelier2.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class FrontController {
	

	@RequestMapping(value = { "/", "/card" }, method = RequestMethod.GET)
	public String index() {
		return "card.html";
	}

	@RequestMapping(value = { "/login" }, method = RequestMethod.GET)
	public String login() {
		return "login.html";
	}

	@RequestMapping(value = { "/addUser" }, method = RequestMethod.GET)
	public String addUser(Model model) {
		return "addUser.html";
	}

	// @RequestMapping(value = { "/card" }, method = RequestMethod.GET)
	// public String card(Model model) {
	// 	return "card.html";
	// }

}
