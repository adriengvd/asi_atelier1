package com.cpe.atelier2.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.JoinColumn;

@Entity
public class Transaction {
	@Id
	@GeneratedValue
    private int id;

    private Date date;

    @OneToOne
    @JoinColumn(name="card_id", nullable=false)
    private Card card;

    @OneToOne
    @JoinColumn(name="buyer_id", nullable=false)
    private User buyer;

    @OneToOne
    @JoinColumn(name="seller_id", nullable=false)
    private User seller;

    private float price;

    public Transaction() {}

	public Transaction(Date date, Card card, User buyer, User seller, float price) {
        this.date = date;
        this.card = card;
        this.buyer = buyer;
        this.seller = seller;
        this.price = price;
	}

    public int getId() {
        return id;
    }

    public User getBuyer() {
        return this.buyer;
    }

    public User getSeller() {
        return this.seller;
    }

    public Date getDate() {
        return this.date;
    }

    public Card getCard() {
        return this.card;
    }

    public float getPrice() {
        return this.price;
    }

}
