package com.cpe.atelier2.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import com.cpe.atelier2.model.Transaction;
import com.cpe.atelier2.repository.TransactionRepository;

@Service
public class TransactionService {
	
	@Autowired
	TransactionRepository transactionRepo;

	public void addTransaction(Transaction transaction) throws IllegalArgumentException {
		transactionRepo.save(transaction);
	}
	
	public Transaction getTransaction(int id) {
		return transactionRepo.findById(id).orElse(null);
	}

	public List<Transaction>  getTransactions() {
		return transactionRepo.findAll();
	}

}
