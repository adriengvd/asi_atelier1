async function send_form(object) {

    
    
    const POST_CARD_URL="/api/user"; 
    let context =   {
                        method: 'POST',
                        headers: { 'Content-Type': 'application/json' },
                        body: JSON.stringify(object)
                    };
    
    fetch(POST_CARD_URL,context)
        .then(async response => {
            if (!response.ok) {
                // get error message from body or default to response status
                const error = (data && data.message) || response.status;
                return Promise.reject(error);
            }
            console.log(response.status);
            if(response.status == 201){

                document.location.href = '/login';

            }
        })
        .catch(error => {
            setErrorMessage(true);
            console.log(error);
        });
}

function getFormData() {

    const form = document.querySelector('#add_user_form');
    const data = Object.fromEntries(new FormData(form).entries());
    console.log(form);
    return {
        "username": data.username,
        "password": data.mdp,
        "rePassword": data.verifmdp,
    }
} 

function verify_form(){

    console.log("sending form");
    let object = getFormData();
    console.log(object);

    if(object.password === object.rePassword){
        setErrorMessage(false);
        send_form(object);
        
    }
    else{
        setErrorMessage(true);

    }

}

function setErrorMessage(show){

    let div = document.querySelector('#errorMessage');
    console.log(div);
    if(show){
        div.classList.add("visible");
    }
    else{
        div.classList.remove("visible");
    }

}