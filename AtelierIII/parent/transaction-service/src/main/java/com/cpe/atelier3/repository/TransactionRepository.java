package com.cpe.atelier3.repository;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

import com.cpe.atelier3.model.Transaction;

public interface TransactionRepository extends CrudRepository<Transaction, Integer> {

	public List<Transaction> findAll();

}
