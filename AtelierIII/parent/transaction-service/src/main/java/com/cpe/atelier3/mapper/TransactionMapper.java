// package com.cpe.atelier3.mapper;

// import java.util.ArrayList;
// import java.util.List;

// import com.cpe.atelier3.model.Transaction;
// import com.cpe.atelier3.model.DTO.TransactionDTO;
// import com.cpe.atelier3.service.CardService;
// import com.cpe.atelier3.service.UserService;

// import org.springframework.beans.BeanUtils;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.stereotype.Service;

// @Service
// public class TransactionMapper {
    
//     @Autowired
//     UserService userService;

//     @Autowired
//     CardService cardTypeService;
    
//     public TransactionDTO convertToTransactionDTO(Transaction transaction) {
//         TransactionDTO transactionDTO = new TransactionDTO();
// 		BeanUtils.copyProperties(transaction, transactionDTO);

//         transactionDTO.setBuyer(transaction.getBuyer().getUsername());
//         transactionDTO.setSeller(transaction.getSeller().getUsername());
//         transactionDTO.setCard(transaction.getCard().getId());
        
//         return transactionDTO;
//     }

//     public List<TransactionDTO> convertToListOfTransactionDTO(List<Transaction> transactions) {
//         List<TransactionDTO> transactionsDTO = new ArrayList<TransactionDTO>();

// 		for (Transaction transaction : transactions) {
// 			transactionsDTO.add(this.convertToTransactionDTO(transaction));
// 		}
//         return transactionsDTO;
//     }

// }
