package com.cpe.atelier3.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.cpe.atelier3.Exception.NotEnoughMoneyException;
import com.cpe.atelier3.Exception.NotFoundException;
import com.cpe.atelier3.Exception.NotSellableException;
import com.cpe.atelier3.Exception.UnauthorizedException;
import com.cpe.atelier3.model.DTO.UserTransactionDTO;
import com.cpe.atelier3.service.TransactionService;

@RestController
@RequestMapping(path = "${apiPrefix}")
public class TransactionRestController {

	@Autowired
	TransactionService transactionService;

	// @Autowired
	// TransactionMapper transactionMapper;
	
	// @RequestMapping(method=RequestMethod.GET,value="/transaction/{id}")
	// public UserTransactionDTO getTransaction(@PathVariable String id) {
	// 	Transaction transaction  = transactionService.getTransaction(Integer.valueOf(id));
	// 	return transactionMapper.convertToTransactionDTO(transaction);
	// }
	
	// @RequestMapping(method=RequestMethod.GET,value="/transactions")
	// public List<UserTransactionDTO> getTransactions() {
	// 	List<Transaction> transactions  = transactionService.getTransactions();
	// 	return transactionMapper.convertToListOfTransactionDTO(transactions);
	// }
	
	// public ResponseEntity<String> buyCard(@CookieValue(value="JWT") String jwt, @PathVariable String id) {

	// 	String connectedUserId = Jwts.parser()
	// 								.setSigningKey(TextCodec.BASE64.decode("TVlTVVBFUlNFQ1JFVEpXVEtFWQ=="))
	// 								.parseClaimsJws(jwt)
	// 								.getBody()
	// 								.getSubject();	


	@RequestMapping(method=RequestMethod.POST,value="/transaction/buy")
	public void buyCard(@CookieValue(value="JWT") String jwt, @RequestBody UserTransactionDTO transactionDTO) {
		try {
			transactionService.buyCard(jwt, transactionDTO);
		} catch (NotFoundException e) {
			e.printStackTrace();
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		} catch (UnauthorizedException | NotSellableException e) {
			e.printStackTrace();
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
		} catch (NotEnoughMoneyException e) {
			e.printStackTrace();
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}

	}

}
