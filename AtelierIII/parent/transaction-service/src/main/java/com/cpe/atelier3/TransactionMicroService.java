package com.cpe.atelier3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;


//@SpringBootApplication(exclude = {SecurityAutoConfiguration.class}, scanBasePackageClasses = {com.cpe.atelier3.client.AuthRestClient.class})
@SpringBootApplication(exclude = {SecurityAutoConfiguration.class})
public class TransactionMicroService {	

	public static void main(String[] args) {
		SpringApplication.run(TransactionMicroService.class, args);
	}

}
