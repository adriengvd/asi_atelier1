package com.cpe.atelier3.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

import com.cpe.atelier3.Exception.NotEnoughMoneyException;
import com.cpe.atelier3.Exception.NotFoundException;
import com.cpe.atelier3.Exception.NotSellableException;
import com.cpe.atelier3.Exception.UnauthorizedException;
import com.cpe.atelier3.client.AuthRestClient;
import com.cpe.atelier3.client.CardRestClient;
import com.cpe.atelier3.client.UserRestClient;
import com.cpe.atelier3.model.Transaction;
import com.cpe.atelier3.model.DTO.CardGetDTO;
import com.cpe.atelier3.model.DTO.JwtDTO;
import com.cpe.atelier3.model.DTO.UpdateBalanceDTO;
import com.cpe.atelier3.model.DTO.UserJwtDTO;
import com.cpe.atelier3.model.DTO.UserTransactionDTO;
import com.cpe.atelier3.repository.TransactionRepository;

@Service
public class TransactionService {
	
	@Autowired
	UserRestClient userService;

	@Autowired
	AuthRestClient authService;

	@Autowired
	CardRestClient cardService;
	
	@Autowired
	TransactionRepository transactionRepo;

	public void addTransaction(Transaction transaction) throws IllegalArgumentException {
		transactionRepo.save(transaction);
	}
	
	public Transaction getTransaction(int id) {
		return transactionRepo.findById(id).orElse(null);
	}

	public List<Transaction>  getTransactions() {
		return transactionRepo.findAll();
	}

	public void buyCard(String jwt, UserTransactionDTO transactionDTO) throws NotFoundException, UnauthorizedException, NotSellableException, NotEnoughMoneyException {
		// Changes the buyer and the seller's balance

		//check if connected user is the same as the transaction
		UserJwtDTO cookieUser = authService.verifyToken(new JwtDTO(jwt));
		if (cookieUser.getId() != transactionDTO.getUserID())
			throw new UnauthorizedException();

		//throws not found exception if cardId does not exists
		CardGetDTO card = cardService.getCard(transactionDTO.getCardID());

		//check if card is sellable
		if (! card.getToSell())
			throw new NotSellableException();

		//check if owner of card is different from transaction user
		if (card.getOwner() == cookieUser.getId())
			throw new UnauthorizedException();

		int sellerID = card.getOwner();
		
		UpdateBalanceDTO sellerBalance = new UpdateBalanceDTO(sellerID, card.getType().getPrice());
		UpdateBalanceDTO buyerBalance = new UpdateBalanceDTO(cookieUser.getId(), - card.getType().getPrice());

		userService.updateBalance(buyerBalance);
		userService.updateBalance(sellerBalance);

		cardService.changeCardOwner(transactionDTO);

		Transaction transaction = new Transaction(LocalDateTime.now(), card.getId(), cookieUser.getId(), sellerID, card.getType().getPrice());
		this.addTransaction(transaction);
	}

}
