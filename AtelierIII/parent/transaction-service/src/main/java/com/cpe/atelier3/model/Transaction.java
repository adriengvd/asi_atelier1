package com.cpe.atelier3.model;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Transaction {
	@Id
	@GeneratedValue
    private int id;

    private LocalDateTime date;
    private int cardId;
    private int buyerId;
    private int sellerId;
    private float price;

    public Transaction() {}

	public Transaction(LocalDateTime date, int cardId, int buyerId, int sellerId, float price) {
        this.date = date;
        this.cardId = cardId;
        this.buyerId = buyerId;
        this.sellerId = sellerId;
        this.price = price;
	}

    public int getId() {
        return id;
    }

    public int getBuyer() {
        return this.buyerId;
    }

    public int getSeller() {
        return this.sellerId;
    }

    public LocalDateTime getDate() {
        return this.date;
    }

    public int getCard() {
        return this.cardId;
    }

    public float getPrice() {
        return this.price;
    }

}
