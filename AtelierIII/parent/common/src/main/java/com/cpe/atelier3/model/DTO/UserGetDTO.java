package com.cpe.atelier3.model.DTO;

public class UserGetDTO {

	private String username;
	private int id;
	private float balance;

	public UserGetDTO() {}

	public UserGetDTO(String username, float balance, int id) {
		this.username = username;
		this.id = id;
		this.balance = balance;
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public float getBalance() {
		return balance;
	}

	public void setBalance(float balance) {
		this.balance = balance;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
