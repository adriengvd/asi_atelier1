package com.cpe.atelier3.client;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

import com.cpe.atelier3.Exception.WrongURLEncodingException;
import com.cpe.atelier3.rest.MicroServiceURLInterface;
import com.cpe.atelier3.rest.TagRestInterface;

public class TagRestClient implements TagRestInterface {

    @Autowired
    MicroServiceURLInterface msURL;
    
    @Override
    public String[] getTagsForImg(String imgURL) throws WrongURLEncodingException {
        int statusCode;
        RestTemplate restTemplate = new RestTemplate();
        String encodedImgURL = encodeURL(imgURL);
        String getURL = msURL.getTagUrl() + GET_TAG_URL + "?url={imgUrl}";
        
		// Send request with GET method.
        ResponseEntity<String[]> response = null;
        try {
            response = restTemplate.getForEntity(getURL, String[].class, encodedImgURL);
        } catch (RestClientResponseException e) {
            statusCode = e.getRawStatusCode();
            if (statusCode == HttpStatus.INTERNAL_SERVER_ERROR.value())
                throw new WrongURLEncodingException();
        }
        return response.getBody();
    }

    private String encodeURL(String url) {
        try {
            return URLEncoder.encode(url, StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
    }
    
}
