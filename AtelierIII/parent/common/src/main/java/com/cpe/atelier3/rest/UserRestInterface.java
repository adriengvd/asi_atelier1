package com.cpe.atelier3.rest;

import com.cpe.atelier3.Exception.NotEnoughMoneyException;
import com.cpe.atelier3.Exception.NotFoundException;
import com.cpe.atelier3.Exception.NotValidPasswordException;
import com.cpe.atelier3.Exception.UnauthorizedException;
import com.cpe.atelier3.model.DTO.UpdateBalanceDTO;
import com.cpe.atelier3.model.DTO.UserGetDTO;
import com.cpe.atelier3.model.DTO.UserJwtDTO;
import com.cpe.atelier3.model.DTO.UserLoginDTO;

import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

public interface UserRestInterface extends APIPrefixInterface {

    public final static String VERIFY_LOGIN_URL = API_PREFIX + "/user/verify-login";
    public final static String UPDATE_USER_BALANCE_URL = API_PREFIX + "/user/update-balance";
    public final static String GET_USER_URL = API_PREFIX + "/user/";


	@RequestMapping(method=RequestMethod.POST,value=VERIFY_LOGIN_URL)
    public UserJwtDTO verifyLogin(UserLoginDTO userDTO ) throws NotFoundException, NotValidPasswordException;
    
    @RequestMapping(method=RequestMethod.POST,value=UPDATE_USER_BALANCE_URL)
    public void updateBalance(UpdateBalanceDTO balanceDTO) throws NotFoundException, NotEnoughMoneyException;
    
    @RequestMapping(method=RequestMethod.GET,value=GET_USER_URL + "{username}")
    public UserGetDTO getUserWithToken(@CookieValue(value="JWT") String jwt, @PathVariable String username) throws UnauthorizedException;

}
