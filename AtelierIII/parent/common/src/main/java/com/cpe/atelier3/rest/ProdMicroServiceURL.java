package com.cpe.atelier3.rest;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("prod")
public class ProdMicroServiceURL implements MicroServiceURLInterface {

    public static String BASE_URL_CARD_MICROSERVICE = "http://nginx_atelier3";
    public static String BASE_URL_TRAN_MICROSERVICE = "http://nginx_atelier3";
    public static String BASE_URL_USER_MICROSERVICE = "http://nginx_atelier3";
    public static String BASE_URL_AUTH_MICROSERVICE = "http://nginx_atelier3";

    // public static String BASE_URL_CARD_MICROSERVICE = "http://card-service:8090";
    // public static String BASE_URL_TRAN_MICROSERVICE = "http://transaction-service:8091";
    // public static String BASE_URL_USER_MICROSERVICE = "http://user-service:8092";
    // public static String BASE_URL_AUTH_MICROSERVICE = "http://auth-service:8093";


    @Override
    public String getCardUrl() {
        return BASE_URL_CARD_MICROSERVICE;
    }

    @Override
    public String getTranUrl() {
        return BASE_URL_TRAN_MICROSERVICE;
    }
    @Override
    public String getUserUrl() {
        return BASE_URL_USER_MICROSERVICE;
    }
    @Override
    public String getAuthUrl() {
        return BASE_URL_AUTH_MICROSERVICE;
    }

    @Override
    public String getTagUrl() {
        return BASE_URL_TAG_MICROSERVICE;
    }
}
