package com.cpe.atelier3.rest;

import java.util.List;

import com.cpe.atelier3.Exception.WrongURLEncodingException;

public interface TagRestInterface extends APIPrefixInterface {
    
    public final static String GET_TAG_URL = API_PREFIX + "/tag";

    public String[] getTagsForImg(String imgURL) throws WrongURLEncodingException;
}
