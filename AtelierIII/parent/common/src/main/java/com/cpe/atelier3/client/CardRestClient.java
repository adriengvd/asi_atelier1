package com.cpe.atelier3.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

import com.cpe.atelier3.Exception.CardNotCreatedException;
import com.cpe.atelier3.Exception.NotFoundException;
import com.cpe.atelier3.Exception.UnauthorizedException;
import com.cpe.atelier3.model.DTO.CardGetDTO;
import com.cpe.atelier3.model.DTO.UserTransactionDTO;
import com.cpe.atelier3.rest.MicroServiceURLInterface;
import com.cpe.atelier3.rest.CardRestInterface;

@Component
public class CardRestClient implements CardRestInterface {

    @Autowired
    MicroServiceURLInterface msURL;

    @Override
    public void initUserCards(int userID) throws CardNotCreatedException {
        int statusCode;

        RestTemplate restTemplate = new RestTemplate();

		// Data attached to the request.
		HttpEntity<Integer> requestBody = new HttpEntity<>(userID);

		// Send request with POST method.
        try {
            restTemplate.postForEntity(msURL.getCardUrl() + INIT_USER_CARDS_URL + userID, requestBody, null);
        } catch (RestClientResponseException e) {
            statusCode = e.getRawStatusCode();
            if (statusCode == HttpStatus.INTERNAL_SERVER_ERROR.value())
                throw new CardNotCreatedException();
        }
        
    }
    
    @Override
    public void changeCardOwner(UserTransactionDTO transactionDTO) {
        int statusCode;

        RestTemplate restTemplate = new RestTemplate();

		// Data attached to the request.
		HttpEntity<UserTransactionDTO> requestBody = new HttpEntity<>(transactionDTO);

		// Send request with POST method.
        try {
            restTemplate.postForEntity(msURL.getCardUrl() + CHANGE_CARD_OWNER_URL, requestBody, null);
        } catch (RestClientResponseException e) {
            statusCode = e.getRawStatusCode();
            if (statusCode == HttpStatus.INTERNAL_SERVER_ERROR.value())
                throw new Error();
        }
    }

    @Override
    public CardGetDTO getCard(int cardID) throws UnauthorizedException, NotFoundException {
        int statusCode;

        RestTemplate restTemplate = new RestTemplate();

        // Send request with GET method.
        CardGetDTO cardDTO = null;
        try {
            cardDTO = restTemplate.getForObject(msURL.getCardUrl() + GET_CARD_URL + cardID, CardGetDTO.class);
        } catch (RestClientResponseException e) {
            statusCode = e.getRawStatusCode();
            if (statusCode == HttpStatus.UNAUTHORIZED.value())
                throw new UnauthorizedException();
            if (statusCode == HttpStatus.NOT_FOUND.value())
                throw new NotFoundException();
        }
        return cardDTO;
    }
    
}
