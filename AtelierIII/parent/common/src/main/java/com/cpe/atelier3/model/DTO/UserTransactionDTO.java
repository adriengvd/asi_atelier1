package com.cpe.atelier3.model.DTO;

public class UserTransactionDTO {

	private int cardID;
	private int userID;
	// private String username;
	// private float amount;

	public UserTransactionDTO() {}

	public UserTransactionDTO(int cardID, int userID) {
	// public UserTransactionDTO(int cardID, String username) {
	// public UserTransactionDTO(int cardID, String username, float amount) {
		this.cardID = cardID;
		this.userID = userID;
		// this.username = username;
		// this.amount = amount;
	}
	
	public int getCardID() {
		return this.cardID;
	}

	public void setCardID(int cardID) {
		this.cardID = cardID;
	}

	public int getUserID() {
		return userID;
	}

	public void setUserID(int userID) {
		this.userID = userID;
	}

	// public float getAmount() {
	// 	return amount;
	// }

	// public void setAmount(float amount) {
	// 	this.amount = amount;
	// }
}
