package com.cpe.atelier3.client;

import com.cpe.atelier3.Exception.NotEnoughMoneyException;
import com.cpe.atelier3.Exception.NotFoundException;
import com.cpe.atelier3.Exception.NotValidPasswordException;
import com.cpe.atelier3.Exception.UnauthorizedException;
import com.cpe.atelier3.model.DTO.UpdateBalanceDTO;
import com.cpe.atelier3.model.DTO.UserGetDTO;
import com.cpe.atelier3.model.DTO.UserJwtDTO;
import com.cpe.atelier3.model.DTO.UserLoginDTO;
import com.cpe.atelier3.rest.MicroServiceURLInterface;
import com.cpe.atelier3.rest.UserRestInterface;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

@Component
public class UserRestClient implements UserRestInterface {

    @Autowired
    MicroServiceURLInterface msURL;
    @Override
    public UserJwtDTO verifyLogin(UserLoginDTO userDTO) throws NotFoundException, NotValidPasswordException {
        int statusCode;

        RestTemplate restTemplate = new RestTemplate();

		// Data attached to the request.
		HttpEntity<UserLoginDTO> requestBody = new HttpEntity<>(userDTO);

		// Send request with POST method.
        ResponseEntity<UserJwtDTO> userJwtDTO = null;
        try {
            userJwtDTO = restTemplate.postForEntity(msURL.getUserUrl() + VERIFY_LOGIN_URL, requestBody, UserJwtDTO.class);
        } catch (RestClientResponseException e) {
            statusCode = e.getRawStatusCode();
            if (statusCode == HttpStatus.NOT_FOUND.value())
                throw new NotFoundException();
            else if (statusCode == HttpStatus.BAD_REQUEST.value())
                throw new NotValidPasswordException();
        }

        return userJwtDTO.getBody();

    }

    @Override
    public void updateBalance(UpdateBalanceDTO balanceDTO) throws NotFoundException, NotEnoughMoneyException {
        int statusCode;

        RestTemplate restTemplate = new RestTemplate();

		// Data attached to the request.
		HttpEntity<UpdateBalanceDTO> requestBody = new HttpEntity<>(balanceDTO);

		// Send request with POST method.
        try {
            restTemplate.postForEntity(msURL.getUserUrl() + UPDATE_USER_BALANCE_URL, requestBody, null);
        } catch (RestClientResponseException e) {
            statusCode = e.getRawStatusCode();
            if (statusCode == HttpStatus.NOT_FOUND.value())
                throw new NotFoundException();
            else if (statusCode == HttpStatus.BAD_REQUEST.value())
                throw new NotEnoughMoneyException();
        }
    }

    @Override
    public UserGetDTO getUserWithToken(@CookieValue(value="JWT") String jwt, @PathVariable String username) throws UnauthorizedException {
        
        int statusCode;

        RestTemplate restTemplate = new RestTemplate();

        // Send request with GET method.
        UserGetDTO userDTO = null;
        try {
            userDTO = restTemplate.getForObject(msURL.getUserUrl() + GET_USER_URL + username, UserGetDTO.class);
        } catch (RestClientResponseException e) {
            statusCode = e.getRawStatusCode();
            if (statusCode == HttpStatus.UNAUTHORIZED.value())
                throw new UnauthorizedException();
        }
        return userDTO;
    }
        
    
    
}
