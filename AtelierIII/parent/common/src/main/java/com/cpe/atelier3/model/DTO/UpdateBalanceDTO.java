package com.cpe.atelier3.model.DTO;

public class UpdateBalanceDTO {
    
    private int userID;
    private float amount;

    public UpdateBalanceDTO() {}

    public UpdateBalanceDTO(int userID, float amount) {
        this.userID = userID;
        this.amount = amount;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int id) {
        this.userID = id;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }
    
}
