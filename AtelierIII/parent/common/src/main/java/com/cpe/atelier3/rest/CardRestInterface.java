package com.cpe.atelier3.rest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.cpe.atelier3.Exception.CardNotCreatedException;
import com.cpe.atelier3.Exception.NotFoundException;
import com.cpe.atelier3.Exception.UnauthorizedException;
import com.cpe.atelier3.model.DTO.CardGetDTO;
import com.cpe.atelier3.model.DTO.UserTransactionDTO;

public interface CardRestInterface extends APIPrefixInterface {
    
    public final static String INIT_USER_CARDS_URL = API_PREFIX + "/card/user/";
    public final static String CHANGE_CARD_OWNER_URL = API_PREFIX + "/card/change-owner";
    public final static String GET_CARD_URL = API_PREFIX + "/card/";

    @RequestMapping(method=RequestMethod.GET,value=GET_CARD_URL + "{cardID}")
    public CardGetDTO getCard(int cardID) throws UnauthorizedException, NotFoundException;

    @RequestMapping(method=RequestMethod.POST,value=INIT_USER_CARDS_URL + "{userID}")
    public void initUserCards(int userID) throws CardNotCreatedException;

    @RequestMapping(method=RequestMethod.POST,value=CHANGE_CARD_OWNER_URL)
    public void changeCardOwner(UserTransactionDTO transactionDTO);

}

