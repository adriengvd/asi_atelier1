package com.cpe.atelier3.model.DTO;

public class UserSignInDTO {

	private String username;
	private String password;
	private String rePassword;

	public UserSignInDTO() {}

	public UserSignInDTO(String username, String password, String rePassword) {
		this.password = password;
		this.rePassword = rePassword;
		this.username = username;
	}
	

	public String getPassword() {
		return password;
	}

	public String getRePassword() {
		return rePassword;
	}

	public String getUsername() {
		return this.username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setRePassword(String rePassword) {
		this.rePassword = rePassword;
	}
}
