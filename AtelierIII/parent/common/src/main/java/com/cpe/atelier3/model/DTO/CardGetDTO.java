package com.cpe.atelier3.model.DTO;

public class CardGetDTO {

    private int id;
    private CardTypeDTO type;
    private boolean toSell;
    private int owner;

	public CardGetDTO() {}

	public CardGetDTO(CardTypeDTO type, boolean toSell, int owner, int id) {
		this.type = type;
        this.toSell = toSell;
        this.owner = owner;
        this.id = id;
	}

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public CardTypeDTO getType() {
        return this.type;
    }

    public void setType(CardTypeDTO type) {
        this.type = type;
    }
    
    public boolean getToSell() {
        return this.toSell;
    }

    public void setToSell(boolean toSell) {
        this.toSell = toSell;
    }

    public int getOwner() {
        return this.owner;
    }

    public void setOwner(int owner) {
        this.owner = owner;
    }
}
