package com.cpe.atelier3.model.DTO;

public class UserLoginDTO {

	private String username;
	private String password;

	public UserLoginDTO() {}

	public UserLoginDTO(String username, String password) {
		this.username = username;
		this.password = password;
	}
	

	public String getPassword() {
		return password;
	}

	public String getUsername() {
		return this.username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
