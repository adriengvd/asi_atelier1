package com.cpe.atelier3.model.DTO;

public class JwtDTO {

	private String jwt;

	public JwtDTO() {}

	public JwtDTO(String jwt) {
		this.jwt = jwt;
	}
	
	public String getJwt() {
		return jwt;
	}
	
	public void setJwt(String jwt) {
		this.jwt = jwt;
	}
}
