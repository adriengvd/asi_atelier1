package com.cpe.atelier3.client;

import com.cpe.atelier3.Exception.UnauthorizedException;
import com.cpe.atelier3.model.DTO.JwtDTO;
import com.cpe.atelier3.model.DTO.UserJwtDTO;
import com.cpe.atelier3.rest.MicroServiceURLInterface;
import com.cpe.atelier3.rest.AuthRestInterface;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

@Component
public class AuthRestClient implements AuthRestInterface {

    @Autowired
    MicroServiceURLInterface msURL;

    @Override
    public UserJwtDTO verifyToken(JwtDTO jwtDTO) throws UnauthorizedException {
        int statusCode;

        RestTemplate restTemplate = new RestTemplate();

		// Data attached to the request.
		HttpEntity<JwtDTO> requestBody = new HttpEntity<>(jwtDTO);

		// Send request with POST method.
        ResponseEntity<UserJwtDTO> userJwtDTO = null;
        try {
            userJwtDTO = restTemplate.postForEntity(msURL.getAuthUrl() + VERIFY_TOKEN_URL, requestBody, UserJwtDTO.class);
        } catch (RestClientResponseException e) {
            statusCode = e.getRawStatusCode();
            if (statusCode == HttpStatus.UNAUTHORIZED.value())
                throw new UnauthorizedException();
        }
        return userJwtDTO.getBody();
    }
    
}
