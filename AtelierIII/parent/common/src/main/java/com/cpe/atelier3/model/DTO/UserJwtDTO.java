package com.cpe.atelier3.model.DTO;

public class UserJwtDTO {

	private String username;
	private int id;

	public UserJwtDTO() {}

	public UserJwtDTO(String username, int id) {
		this.username = username;
		this.id = id;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}

	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
}
