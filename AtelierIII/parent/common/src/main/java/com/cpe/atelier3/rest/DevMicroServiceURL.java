package com.cpe.atelier3.rest;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("dev")
public class DevMicroServiceURL implements MicroServiceURLInterface {
    
    public static String BASE_URL_CARD_MICROSERVICE = "http://localhost:8090";
    public static String BASE_URL_TRAN_MICROSERVICE = "http://localhost:8091";
    public static String BASE_URL_USER_MICROSERVICE = "http://localhost:8092";
    public static String BASE_URL_AUTH_MICROSERVICE = "http://localhost:8093";
    public static String BASE_URL_TAG_MICROSERVICE = "http://localhost:8007";

    @Override
    public String getCardUrl() {
        return BASE_URL_CARD_MICROSERVICE;
    }

    @Override
    public String getTranUrl() {
        return BASE_URL_TRAN_MICROSERVICE;
    }
    @Override
    public String getUserUrl() {
        return BASE_URL_USER_MICROSERVICE;
    }
    @Override
    public String getAuthUrl() {
        return BASE_URL_AUTH_MICROSERVICE;
    }

    @Override
    public String getTagUrl() {
        return BASE_URL_TAG_MICROSERVICE;
    }

}
