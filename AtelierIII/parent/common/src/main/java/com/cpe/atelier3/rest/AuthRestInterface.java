package com.cpe.atelier3.rest;

import com.cpe.atelier3.Exception.UnauthorizedException;
import com.cpe.atelier3.model.DTO.JwtDTO;
import com.cpe.atelier3.model.DTO.UserJwtDTO;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

public interface AuthRestInterface extends APIPrefixInterface {

    public final static String VERIFY_TOKEN_URL = API_PREFIX + "/auth/verify-token";

	@RequestMapping(method=RequestMethod.POST,value=VERIFY_TOKEN_URL)
    public UserJwtDTO verifyToken(JwtDTO jwtDTO) throws UnauthorizedException; 
}
