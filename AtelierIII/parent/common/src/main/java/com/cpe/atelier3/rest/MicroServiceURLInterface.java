package com.cpe.atelier3.rest;

import org.springframework.stereotype.Service;

@Service
public interface MicroServiceURLInterface {

    // LOCALHOST
    public static String BASE_URL_CARD_MICROSERVICE = "";
    public static String BASE_URL_TRAN_MICROSERVICE = "";
    public static String BASE_URL_USER_MICROSERVICE = "";
    public static String BASE_URL_AUTH_MICROSERVICE = "";
    public static String BASE_URL_TAG_MICROSERVICE = "";

    public String getCardUrl();
    public String getTranUrl();
    public String getUserUrl();
    public String getAuthUrl();
    public String getTagUrl();

}
