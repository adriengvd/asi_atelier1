package com.cpe.atelier3.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import io.jsonwebtoken.JwtException;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import com.cpe.atelier3.Exception.NotFoundException;
import com.cpe.atelier3.Exception.NotValidPasswordException;
import com.cpe.atelier3.model.DTO.JwtDTO;
import com.cpe.atelier3.model.DTO.UserCookieDTO;
import com.cpe.atelier3.model.DTO.UserLoginDTO;
import com.cpe.atelier3.model.DTO.UserJwtDTO;
import com.cpe.atelier3.service.AuthService;

@RestController
// @RequestMapping(path = "${apiPrefix}")
public class AuthRestController implements AuthRestInterface{

	public final static String POST_LOGIN_URL = "/api/auth/login";
	public final static String POST_LOGOUT_URL = "/api/auth/logout";
	public final static String POST_IS_LOGGED_IN_URL = "/api/auth/is-logged-in";

	@Autowired
	AuthService authService;
	
	@RequestMapping(method=RequestMethod.POST,value=POST_LOGIN_URL)
	public ResponseEntity<?> login(@RequestBody UserLoginDTO userLoginDTO, HttpServletResponse response) {
		UserCookieDTO userCookieDTO;
		try {
			userCookieDTO = authService.loginUser(userLoginDTO);
		} catch (NotFoundException e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		} catch (NotValidPasswordException e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}

		Cookie jwtCookie = this.createJWTCookie(userCookieDTO.getToken());
		Cookie userCookie = this.createUserInfoCookie(userCookieDTO.getUsername(), userCookieDTO.getId());
		response.addCookie(jwtCookie);
		response.addCookie(userCookie);
		return ResponseEntity.status(HttpStatus.OK).build();
	}

	@RequestMapping(method=RequestMethod.GET,value=POST_LOGOUT_URL)
	public ResponseEntity<?> logout(HttpServletResponse response) {
		Cookie cookie = this.createJWTCookie(null);
		response.addCookie(cookie);
		return ResponseEntity.status(HttpStatus.OK).build();
	}

	@RequestMapping(method=RequestMethod.POST,value=POST_IS_LOGGED_IN_URL)
	public ResponseEntity<?> islogin(@CookieValue(value="JWT") String jwt) {
		UserJwtDTO userJwtDTO;
		try {
			userJwtDTO = authService.verifyToken(jwt);
		} catch (JwtException e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		return ResponseEntity.status(HttpStatus.OK).build();
	}

	private Cookie createJWTCookie(String token) {
		Cookie cookie = new Cookie("JWT", token);
		cookie.setHttpOnly(true);
		cookie.setSecure(false);
		cookie.setDomain("localhost");
		cookie.setPath("/");
		return cookie;
	}

	private Cookie createUserInfoCookie(String username, int id) {
		String cookieInfo ="username=" + username + "&id=" + id;
		Cookie cookie = new Cookie("userInfo", cookieInfo);
		cookie.setHttpOnly(false);
		cookie.setSecure(false);
		cookie.setDomain("localhost");
		cookie.setPath("/");
		return cookie;
	}

	@Override
	public UserJwtDTO verifyToken(@RequestBody JwtDTO jwtDTO) {
		UserJwtDTO userJwtDTO;
		try {
			userJwtDTO = authService.verifyToken(jwtDTO.getJwt());
		} catch (JwtException e) {
			throw new ResponseStatusException(HttpStatus.FORBIDDEN);
		}
		return userJwtDTO;
	}
}
