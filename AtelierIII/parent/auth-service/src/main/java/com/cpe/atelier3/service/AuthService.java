package com.cpe.atelier3.service;

import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Service;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.TextCodec;

import java.util.Calendar;
import java.util.Date;

import com.cpe.atelier3.Exception.NotFoundException;
import com.cpe.atelier3.Exception.NotValidPasswordException;
import com.cpe.atelier3.client.UserRestClient;
import com.cpe.atelier3.model.DTO.UserLoginDTO;
import com.cpe.atelier3.model.DTO.UserCookieDTO;
import com.cpe.atelier3.model.DTO.UserJwtDTO;

@Service
// @SpringBootApplication(scanBasePackageClasses = {com.cpe.atelier3.client.UserRestClient.class})
public class AuthService {

	private static int JWTHourExpiration = 3;

	private static String JWTBase64Key = "TVlTVVBFUlNFQ1JFVEpXVEtFWQ==";

	@Autowired
	UserRestClient userService;

	public UserCookieDTO loginUser(UserLoginDTO userDTO) throws NotFoundException, NotValidPasswordException {

		UserJwtDTO userJwtDTO = userService.verifyLogin(userDTO);

		String token = this.generateJWTToken(userJwtDTO);

		return new UserCookieDTO(userJwtDTO.getUsername(), userJwtDTO.getId(), token);
	}

	private String generateJWTToken(UserJwtDTO user) {

		Date issuedAt = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(issuedAt);
		c.add(Calendar.HOUR, JWTHourExpiration);
		Date Expiration = c.getTime();

		return Jwts.builder()
				.setIssuer("SpCardApp")
				.setSubject(user.getUsername())
				.claim("username", user.getUsername())
				.claim("id", user.getId())
				.setIssuedAt(issuedAt)
				.setExpiration(Expiration)
				.signWith(
					SignatureAlgorithm.HS256,
					TextCodec.BASE64.decode(JWTBase64Key)
				)
				.compact();
	}

	public UserJwtDTO verifyToken(String token) {
		Claims body;
		String currentUserUsername;
		int userID;
		try {
			body = Jwts.parser()
					.setSigningKey(TextCodec.BASE64.decode(JWTBase64Key))
					.parseClaimsJws(token)
					.getBody();
			currentUserUsername = body.getSubject();
			userID = body.get("id", Integer.class);							
		} catch (JwtException e) {
			throw e;
		}
		return new UserJwtDTO(currentUserUsername, userID);
	}


}
