package com.cpe.atelier3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


// @SpringBootApplication(scanBasePackageClasses = {com.cpe.atelier3.client.UserRestClient.class})
@SpringBootApplication()
public class AuthMicroService {	

	public static void main(String[] args) {
		SpringApplication.run(AuthMicroService.class, args);
	}

}
