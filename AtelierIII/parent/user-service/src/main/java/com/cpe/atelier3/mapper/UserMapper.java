package com.cpe.atelier3.mapper;

import com.cpe.atelier3.model.User;
import com.cpe.atelier3.model.DTO.UserLoginDTO;
import com.cpe.atelier3.model.DTO.UserSignInDTO;
import com.cpe.atelier3.model.DTO.UserGetDTO;
import com.cpe.atelier3.model.DTO.UserJwtDTO;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

@Service
public class UserMapper {
    
    public User convertFromUserLoginDTO(UserLoginDTO userDTO) {
        User user = new User();
		BeanUtils.copyProperties(userDTO, user);

        return user;
    }

    public User convertFromUserSignInDTO(UserSignInDTO userDTO) {
        User user = new User();
		BeanUtils.copyProperties(userDTO, user);

        return user;
    }

    public UserGetDTO convertToUserGetDTO(User user) {
        UserGetDTO userGetDTO = new UserGetDTO();
        BeanUtils.copyProperties(user, userGetDTO);

        return userGetDTO;
    }

    public UserJwtDTO convertToUserJwtDTO(User user) {
        UserJwtDTO userJwtDTO = new UserJwtDTO();
        BeanUtils.copyProperties(user, userJwtDTO);

        return userJwtDTO;
    }
}
