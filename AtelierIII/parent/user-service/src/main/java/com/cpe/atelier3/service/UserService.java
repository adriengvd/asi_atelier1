package com.cpe.atelier3.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.cpe.atelier3.Exception.CardNotCreatedException;
import com.cpe.atelier3.Exception.NotEnoughMoneyException;
import com.cpe.atelier3.Exception.NotFoundException;
import com.cpe.atelier3.Exception.NotValidPasswordException;
import com.cpe.atelier3.Exception.UnauthorizedException;
import com.cpe.atelier3.client.AuthRestClient;
import com.cpe.atelier3.client.CardRestClient;
import com.cpe.atelier3.mapper.UserMapper;
import com.cpe.atelier3.model.User;
import com.cpe.atelier3.model.DTO.JwtDTO;
import com.cpe.atelier3.model.DTO.UpdateBalanceDTO;
import com.cpe.atelier3.model.DTO.UserJwtDTO;
import com.cpe.atelier3.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	UserRepository userRepo;

	@Autowired
	UserMapper userMapper;

	@Autowired
	AuthRestClient authService;
	
	@Autowired
	CardRestClient cardService;

	// Used for unit testing
	public UserService(UserRepository userRepo, CardRestClient cardService, UserMapper userMapper) {
		this.userRepo = userRepo;
		this.cardService = cardService;
		this.userMapper = userMapper;
	}

	public void addUser(User user) throws IllegalArgumentException, CardNotCreatedException {
		this.setNewUserBalance(user);
		this.encryptPassword(user);
		User savedUser;
		try {
			savedUser = userRepo.save(user);
		} catch (IllegalArgumentException e) {
			throw e;
		}
		
		try {
			cardService.initUserCards(savedUser.getId());
		} catch (CardNotCreatedException e) {
			userRepo.delete(savedUser);
			throw e;
		}
	}

	public User getUser(String username) {
		return userRepo.findByUsername(username).orElse(null);
	}
	
	public User getUserWithToken(String username, String jwt) throws UnauthorizedException {
		JwtDTO jwtDTO = new JwtDTO(jwt);
		UserJwtDTO userJwtDTO = authService.verifyToken(jwtDTO);
		if (!userJwtDTO.getUsername().equals(username)) {
			throw new UnauthorizedException();
		}
		return userRepo.findByUsername(username).orElse(null);
	}

	public UserJwtDTO loginUser(String username, String loginPassword) throws NotFoundException, NotValidPasswordException {
		User user = this.getUser(username);

		if (user == null) {
			throw new NotFoundException();
		}

		if (! this.doesPasswordsMatch(user, loginPassword)) {
			throw new NotValidPasswordException();
		}
		return userMapper.convertToUserJwtDTO(user);
	}
	
	public void updateBalance(UpdateBalanceDTO balanceDTO) throws NotFoundException, NotEnoughMoneyException {
		User user = userRepo.findById(balanceDTO.getUserID()).orElse(null);

		if (user == null)
			throw new NotFoundException();

		float newBalance = user.getBalance() + balanceDTO.getAmount();
		
		if (newBalance < 0) {
			throw new NotEnoughMoneyException();
		}

		user.setBalance(newBalance);
		System.out.println("User : " + user.getUsername() + ", new balance : " + user.getBalance());
		user = userRepo.save(user);
		System.out.println("After save : User : " + user.getUsername() + ", new balance : " + user.getBalance());

	}

	private boolean doesPasswordsMatch(User user, String loginPassword) {
		PasswordEncoder encoder = new BCryptPasswordEncoder();
		return encoder.matches(loginPassword, user.getPassword());
	}


	private void setNewUserBalance(User user) {
		float balance = 5000f;
		user.setBalance(balance);
	}

	private void encryptPassword(User user) {
		PasswordEncoder encoder = new BCryptPasswordEncoder();
		String password = user.getPassword();
		user.setPassword(encoder.encode(password));
	}

}
