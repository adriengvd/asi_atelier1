package com.cpe.atelier3.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.cpe.atelier3.Exception.CardNotCreatedException;
import com.cpe.atelier3.Exception.NotEnoughMoneyException;
import com.cpe.atelier3.Exception.NotFoundException;
import com.cpe.atelier3.Exception.NotValidPasswordException;
import com.cpe.atelier3.Exception.UnauthorizedException;
import com.cpe.atelier3.mapper.UserMapper;
import com.cpe.atelier3.model.User;
import com.cpe.atelier3.model.DTO.UserSignInDTO;
import com.cpe.atelier3.model.DTO.UpdateBalanceDTO;
import com.cpe.atelier3.model.DTO.UserGetDTO;
import com.cpe.atelier3.model.DTO.UserJwtDTO;
import com.cpe.atelier3.model.DTO.UserLoginDTO;
import com.cpe.atelier3.service.UserService;

@RestController
public class UserRestController implements UserRestInterface {

	public final static String POST_USER_URL = API_PREFIX + "/user";
	//public final static String GET_USER_URL = API_PREFIX + "/user/";

	@Autowired
	UserService userService;

	@Autowired
	UserMapper userMapper;
	
	@RequestMapping(method=RequestMethod.POST,value=POST_USER_URL)
	@ResponseStatus(code = HttpStatus.CREATED)
	public void postUser(@RequestBody UserSignInDTO userDTO) {

		if (userService.getUser(userDTO.getUsername()) != null) {
			throw new ResponseStatusException(HttpStatus.CONFLICT);
		}

		if (! userDTO.getPassword().equals(userDTO.getRePassword())) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}

		try {
			userService.addUser(userMapper.convertFromUserSignInDTO(userDTO));
		} catch (IllegalArgumentException | CardNotCreatedException e) {
			e.printStackTrace();
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@Override
	public UserGetDTO getUserWithToken(@CookieValue(value="JWT") String jwt, @PathVariable String username) {
		User user;
		try {
			user = userService.getUserWithToken(username, jwt);
		} catch (UnauthorizedException e) {
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
		}
		return userMapper.convertToUserGetDTO(user);
	}
	

	@Override
	public UserJwtDTO verifyLogin(@RequestBody UserLoginDTO userDTO) throws NotFoundException, NotValidPasswordException {
		
		UserJwtDTO userJwtDTO;
		try {
			userJwtDTO = userService.loginUser(userDTO.getUsername(), userDTO.getPassword());
		} catch (NotFoundException e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		} catch (NotValidPasswordException e) {
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
		}
		return userJwtDTO;
	}

	@Override
	public void updateBalance(@RequestBody UpdateBalanceDTO balanceDTO) {

		try {
			userService.updateBalance(balanceDTO);
		} catch (NotFoundException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		} catch (NotEnoughMoneyException e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}
		
	}


	
}
