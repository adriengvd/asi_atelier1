package com.cpe.atelier3;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.BeanUtils;

import com.cpe.atelier3.Exception.CardNotCreatedException;
import com.cpe.atelier3.Exception.NotEnoughMoneyException;
import com.cpe.atelier3.Exception.NotFoundException;
import com.cpe.atelier3.Exception.NotValidPasswordException;
import com.cpe.atelier3.client.CardRestClient;
import com.cpe.atelier3.mapper.UserMapper;
import com.cpe.atelier3.model.User;
import com.cpe.atelier3.model.DTO.UpdateBalanceDTO;
import com.cpe.atelier3.model.DTO.UserJwtDTO;
import com.cpe.atelier3.repository.UserRepository;
import com.cpe.atelier3.service.UserService;
 
@ExtendWith(MockitoExtension.class)
 
// Main class
class UserTest {

    @Mock
    private UserRepository userRepo;

    @Mock
    private CardRestClient cardService;

    @Mock
    private UserMapper userMapper;

    private UserService userService;
 
    @BeforeEach 
    public void setUp() {
        this.userService = new UserService(userRepo, cardService, userMapper);
    }
 
    
    // Test logging in a user with valid and invalid usernames/passwords
    @Test 
    public void verifyLoginTest() throws IllegalArgumentException, CardNotCreatedException, NotFoundException, NotValidPasswordException {
        User user = new User("username", "password");
        when(userRepo.save(user)).thenReturn(user);
        when(userRepo.findByUsername("username")).thenReturn(Optional.of(user));

        UserJwtDTO userJwtDTO = new UserJwtDTO();
        BeanUtils.copyProperties(user, userJwtDTO);
        when(userMapper.convertToUserJwtDTO(user)).thenReturn(userJwtDTO);

        userService.addUser(user);
        
        assertThrows(NotValidPasswordException.class,() -> userService.loginUser("username", "wrongPassword") );
        assertThrows(NotFoundException.class,() -> userService.loginUser("inexistantUsername", "password") );
        UserJwtDTO userDTO = userService.loginUser("username", "password");
        assertEquals(userDTO.getUsername(), user.getUsername());
        assertEquals(userDTO.getId(), user.getId());
    }


    // Test a user's starting balance, then adding / removing from it
    @Test
    public void updateBalanceTest() throws IllegalArgumentException, CardNotCreatedException, NotFoundException, NotEnoughMoneyException {
        User user = new User("username", "password");
        when(userRepo.save(user)).thenReturn(user);
        userService.addUser(user);
        lenient().when(userRepo.findById(user.getId())).thenReturn(Optional.of(user));

        assertEquals(user.getBalance(), 5000f);

        UpdateBalanceDTO balanceDTO = new UpdateBalanceDTO(user.getId()+1, 100f);
        assertThrows(NotFoundException.class,() -> userService.updateBalance(balanceDTO));
        balanceDTO.setUserID(user.getId());
        balanceDTO.setAmount(-10000f);
        assertThrows(NotEnoughMoneyException.class,() -> userService.updateBalance(balanceDTO));
        balanceDTO.setAmount(500f);
        userService.updateBalance(balanceDTO);
        assertEquals(user.getBalance(), 5500f);
    }

}