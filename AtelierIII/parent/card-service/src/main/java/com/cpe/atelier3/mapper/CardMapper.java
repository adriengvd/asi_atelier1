package com.cpe.atelier3.mapper;

import com.cpe.atelier3.model.Card;
import com.cpe.atelier3.model.CardType;
import com.cpe.atelier3.model.DTO.CardGetDTO;
import com.cpe.atelier3.model.DTO.CardPostDTO;
import com.cpe.atelier3.model.DTO.CardTypeDTO;
import com.cpe.atelier3.service.CardTypeService;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import org.springframework.web.server.ResponseStatusException;

@Service
public class CardMapper {

    @Autowired
    CardTypeService cardTypeService;

    public Card convertFromPostDTO(CardPostDTO cardDTO) throws ResponseStatusException {
        Card card = new Card();
		BeanUtils.copyProperties(cardDTO, card);

		CardType cardType = cardTypeService.getCardType(cardDTO.getType());

		if (cardType == null) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No CardType with this id");
		}

		card.setOwnerID(cardDTO.getOwner());
		card.setType(cardType);

        return card;
    }

    public CardGetDTO convertToGetCard(Card card) {
        CardGetDTO dto = new CardGetDTO();
        CardTypeDTO typeDTO = new CardTypeDTO();

        BeanUtils.copyProperties(card, dto);
        BeanUtils.copyProperties(card.getType(), typeDTO);
        dto.setOwner(card.getOwnerID());
        dto.setType(typeDTO);
        
        return dto;
    }

    public List<CardGetDTO> convertToListOfGetCards(List<Card> cards) {
        List<CardGetDTO> cardsDTO = new ArrayList<CardGetDTO>();

		for (Card card : cards) {
			cardsDTO.add(this.convertToGetCard(card));
		}
        return cardsDTO;
    }
}
