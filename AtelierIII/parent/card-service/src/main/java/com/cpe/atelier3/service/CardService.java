package com.cpe.atelier3.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import com.cpe.atelier3.Exception.AlreadySellableException;
import com.cpe.atelier3.Exception.NotFoundException;
import com.cpe.atelier3.Exception.UnauthorizedException;
import com.cpe.atelier3.client.AuthRestClient;
import com.cpe.atelier3.client.UserRestClient;
import com.cpe.atelier3.model.Card;
import com.cpe.atelier3.model.CardType;
import com.cpe.atelier3.model.DTO.UserTransactionDTO;
import com.cpe.atelier3.model.DTO.JwtDTO;
import com.cpe.atelier3.model.DTO.UserJwtDTO;
import com.cpe.atelier3.repository.CardRepository;

@Service
public class CardService {

	@Autowired
	CardRepository cardRepo;

	@Autowired
	CardTypeService cardTypeService;

	@Autowired
	UserRestClient userService;

	@Autowired
	AuthRestClient authService;

	// public void addCard(Card card) throws IllegalArgumentException {
	// 	cardRepo.save(card);
	// }
	
	public Card getCard(int id) throws NotFoundException {
		Card card = cardRepo.findById(id).orElse(null);

		if (card == null)
			throw new NotFoundException();
		
		return card;
	}
	
	public void sellCard(String jwt, int id) throws NotFoundException, AlreadySellableException, UnauthorizedException {
		Card card = cardRepo.findById(id).orElse(null);

		if (card == null)
			throw new NotFoundException();

		UserJwtDTO cookieUser = authService.verifyToken(new JwtDTO(jwt));
		// The user has to own the card to make it sellable
		if (cookieUser.getId() != card.getOwnerID())
			throw new UnauthorizedException();
		else if (card.getToSell())
			throw new AlreadySellableException();

		card.setToSell(true);
		cardRepo.save(card);
		
	}

	public List<Card> getCards() {
		return (List<Card>) cardRepo.findAll();
	}


	public void changeCardOwner(UserTransactionDTO transactionDTO) throws NotFoundException {
		Card card = cardRepo.findById(transactionDTO.getCardID()).orElse(null);

		if (card == null) {
			throw new NotFoundException();
		}
		
		card.setOwnerID(transactionDTO.getUserID());
		cardRepo.save(card);
	}

	public List<Card> getUserCards(int userID, String jwt) throws UnauthorizedException {
		UserJwtDTO user = authService.verifyToken(new JwtDTO(jwt));
		if (userID != user.getId())
			throw new UnauthorizedException();
		return (List<Card>) cardRepo.findByOwnerID(user.getId());
	}

	// Get all buyable cards except the connected user's (toSell = true)
	public List<Card> getUserBuyableCards(String jwt) throws UnauthorizedException {
		UserJwtDTO user = authService.verifyToken(new JwtDTO(jwt));
		List<Card> cards = null;

		cards = cardRepo.findByOwnerIDNotAndToSell(user.getId(), true);

		return cards;
	}

	// Get all the connected user's sellable cards (toSell = false)
	public List<Card> getUserSellableCards(String jwt) throws UnauthorizedException {
		UserJwtDTO user = authService.verifyToken(new JwtDTO(jwt));
		List<Card> cards = null;

		cards = cardRepo.findByOwnerIDAndToSell(user.getId(), false);

		return cards;
	}
	
	public void initUserCards(int userID) throws IllegalArgumentException {
		List<CardType> lastFiveCardTypes = cardTypeService.getLastFiveCardTypes();
		Card card;
		for (CardType cardType : lastFiveCardTypes) {
			card = new Card(cardType, userID);
			try {
				cardRepo.save(card);
			} catch (IllegalArgumentException e) {
				throw e;
			}
		}
	}

}
