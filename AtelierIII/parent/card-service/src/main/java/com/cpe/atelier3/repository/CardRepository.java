package com.cpe.atelier3.repository;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

import com.cpe.atelier3.model.Card;
// import com.cpe.atelier3.model.User;

public interface CardRepository extends CrudRepository<Card, Integer> {

    List<Card> findByOwnerID(int ownerID);

    // @Query(value = "SELECT * from card_type order by id desc limit 5", nativeQuery = true)
    // List<CardType> findLastFiveCardTypes();

    List<Card> findByOwnerIDNotAndToSell(int ownerId, boolean toSell);

    List<Card> findByOwnerIDAndToSell(int ownerId, boolean toSell);

}
