package com.cpe.atelier3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication()
public class CardMicroService {
    
    public static void main(String[] args) {
		SpringApplication.run(CardMicroService.class, args);
	}

}
