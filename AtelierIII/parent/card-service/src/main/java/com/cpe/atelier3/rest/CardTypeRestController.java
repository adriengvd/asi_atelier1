package com.cpe.atelier3.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cpe.atelier3.model.CardType;
import com.cpe.atelier3.service.CardTypeService;

@RestController
// @RequestMapping(path = "${apiPrefix}")
public class CardTypeRestController {

	public static final String POST_CARD_TYPE_URL = "/api/card-type";

	@Autowired
	CardTypeService cardTypeService;
	
	@RequestMapping(method=RequestMethod.POST,value=POST_CARD_TYPE_URL)
	public ResponseEntity<String> addCard(@RequestBody CardType cardType) {

		try {
			cardTypeService.addCardType(cardType);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
		}

		return ResponseEntity.status(HttpStatus.CREATED).body(null);
	}
}
