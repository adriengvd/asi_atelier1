package com.cpe.atelier3.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.cpe.atelier3.Exception.AlreadySellableException;
import com.cpe.atelier3.Exception.NotFoundException;
import com.cpe.atelier3.Exception.UnauthorizedException;
import com.cpe.atelier3.mapper.CardMapper;
import com.cpe.atelier3.model.Card;
import com.cpe.atelier3.model.DTO.CardGetDTO;
import com.cpe.atelier3.model.DTO.UserTransactionDTO;
import com.cpe.atelier3.service.CardService;
import com.cpe.atelier3.service.CardTypeService;

@RestController
//@RequestMapping(path = "${apiPrefix}")
public class CardRestController implements CardRestInterface{

	public static final String GET_CARD_URL = "/api/card/";
	public static final String GET_CARDS_URL = "/api/card";
	public static final String POST_CARD_URL = "/api/card";
	public static final String SELL_CARD_URL = "/api/card/sell/";
	public static final String GET_USER_CARD_URL = "/api/card/user/";
	public static final String GET_USER_BUYABLE_CARDS_URL = "/api/card/user/buy";
	public static final String GET_USER_SELLABLE_CARDS_URL = "/api/card/user/sell";

	@Autowired
	CardService cardService;

	@Autowired
	CardTypeService cardTypeService;
	
	@Autowired
	CardMapper cardMapper;
	
	// @RequestMapping(method=RequestMethod.POST,value=POST_CARD_URL)
	// public ResponseEntity<String> addCard(@RequestBody CardPostDTO cardDTO) {
	// 	try {
	// 		Card card = cardMapper.convertFromPostDTO(cardDTO);
	// 		cardService.addCard(card);
	// 	} catch (ResponseStatusException e) {
	// 		e.printStackTrace();
	// 		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
	// 	}
	// 	 catch(IllegalArgumentException e) {
	// 		e.printStackTrace();
	// 		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
	// 	}

	// 	return ResponseEntity.status(HttpStatus.CREATED).body(null);
	// }
	
	@Override
	public CardGetDTO getCard(@PathVariable int cardID) {
		Card card;
		try {
			card = cardService.getCard(cardID);
		} catch (NotFoundException e) {
			e.printStackTrace();
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
		return cardMapper.convertToGetCard(card);
	}

	@RequestMapping(method=RequestMethod.GET, value=GET_CARDS_URL)
	public List<CardGetDTO> getCards() {
		List<Card> cards = cardService.getCards();
		return cardMapper.convertToListOfGetCards(cards);
	}

	// Get connected user cards
	@RequestMapping(method=RequestMethod.GET, value=GET_USER_CARD_URL + "{id}")
	public List<CardGetDTO> getUserCards(@CookieValue("JWT") String jwt, @PathVariable int id) {
		List<Card> cards = null;
		try {
			cards = cardService.getUserCards(id, jwt);
		} catch (UnauthorizedException e) {
			e.printStackTrace();
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
		}
		return cardMapper.convertToListOfGetCards(cards);
	}

	// Get all buyable cards except the connected user's (toSell = true)
	@RequestMapping(method=RequestMethod.GET, value=GET_USER_BUYABLE_CARDS_URL)
	public List<CardGetDTO> getUserBuyableCards(@CookieValue("JWT") String jwt) {
		List<Card> cards = null;
		try {
			cards = cardService.getUserBuyableCards(jwt);
		} catch (UnauthorizedException e) {
			e.printStackTrace();
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
		}
		return cardMapper.convertToListOfGetCards(cards);
	}

	// Get all the connected user's sellable cards (toSell = false)
	@RequestMapping(method=RequestMethod.GET, value=GET_USER_SELLABLE_CARDS_URL)
	public List<CardGetDTO> getUserSellableCards(@CookieValue("JWT") String jwt) {
		List<Card> cards = null;
		try {
			cards = cardService.getUserSellableCards(jwt);
		} catch (UnauthorizedException e) {
			e.printStackTrace();
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
		}
		return cardMapper.convertToListOfGetCards(cards);
	}

	@RequestMapping(method=RequestMethod.POST, value=SELL_CARD_URL + "{id}")
	public void sellCard(@CookieValue(value="JWT") String jwt, @PathVariable int id) {
		try {
			cardService.sellCard(jwt, id);
		} catch (NotFoundException e) {
			e.printStackTrace();
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		} catch (AlreadySellableException e) {
			e.printStackTrace();
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		} catch (UnauthorizedException e) {
			e.printStackTrace();
			throw new ResponseStatusException(HttpStatus.FORBIDDEN);
		}
	}


	@Override
	@ResponseStatus(HttpStatus.CREATED)
	public void initUserCards(@PathVariable int userID) {
		try {
			cardService.initUserCards(userID);
		} catch (IllegalArgumentException e) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	public void changeCardOwner(@RequestBody UserTransactionDTO transactionDTO) {
		
		try {
			cardService.changeCardOwner(transactionDTO);
		} catch (NotFoundException e) {
			e.printStackTrace();
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}
	}



}
