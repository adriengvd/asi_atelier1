package com.cpe.atelier3.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import com.cpe.atelier3.service.CardTypeService;

@Component
public class DataLoader implements ApplicationRunner {

    @Autowired
    CardTypeService cardTypeService;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        cardTypeService.initCardTypes();
    }
    
}
