package com.cpe.atelier3.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;

@Entity
public class Card {
	@Id
	@GeneratedValue
    private int id;

    @ManyToOne
    @JoinColumn(name="type_id", nullable=false)
    private CardType type;
    private boolean toSell;

    private int ownerID;

    public Card() {}

	public Card(CardType cardType, int ownerID) {
		this.type = cardType;
        this.toSell = false;
        this.ownerID = ownerID;
	}

    public int getId() {
        return this.id;
    }

    public CardType getType() {
        return this.type;
    }

    public void setType(CardType type) {
        this.type = type;
    }

    public Boolean getToSell() {
        return this.toSell;
    }

    public void setToSell(boolean toSell) {
        this.toSell = toSell;
    }

    public int getOwnerID() {
        return ownerID;
    }

    public void setOwnerID(int ownerID) {
        this.ownerID = ownerID;
    }

    public String toString() {
        String response;
        response = "isSellable: " + this.getToSell();
        if (this.ownerID <= 0) {
            response += ", Owner: " + this.ownerID;
        } 
        if (this.type != null) {
            response += ", CardType: " + this.type.getId();
        } 

        return response;
    }
}
