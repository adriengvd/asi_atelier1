import numpy as np
import tensorflow as tf
import cv2
import flask
from flask import Flask, flash, request, redirect, url_for,send_from_directory
import json
from werkzeug.utils import secure_filename
import os
import urllib.request


UPLOAD_FOLDER = '/img'
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])
app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

def read_label_map(label_map_path, id):

    item_id = None
    item_name = None
    item_name_display= None
    items = {}
    
    with open(label_map_path, "r") as file:
        for line in file:
            line.replace(" ", "")
            if line == "item{":
                pass
            elif line == "}":
                pass
            elif "id" in line:
                item_id = int(line.split(":", 1)[1].strip())
            elif "display_name" in line:
                item_name_display = line.split(":", 1)[1].replace("\"", "").strip()
                if item_id == id:
                    print(item_name_display)
                    return item_name_display
            elif "name" in line:
                item_name = line.split(":", 1)[1].replace("'", "").strip()



    return item_name

def get_tag(image_path):
    # load model from path
    model= tf.saved_model.load("faster_rcnn/saved_model")
    # read image and preprocess
    img = cv2.imread(image_path)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    input_tensor = np.expand_dims(img, 0)
    tags= []
    
    # predict from model
    resp = model(input_tensor)
    # iterate over boxes, class_index and score list
    for boxes, classes, scores in zip(resp['detection_boxes'].numpy(), resp['detection_classes'], resp['detection_scores'].numpy()):
        for box, cls, score in zip(boxes, classes, scores): # iterate over sub values in list
            if score > 0.8: # we are using only detection with confidence of over 0.8
                h = img.shape[0]
                w = img.shape[1]
                ymin = int(box[0] * h)
                xmin = int(box[1] * w)
                ymax = int(box[2] * h)
                xmax = int(box[3] * w)
                # write classname for bounding box
                tags.append(read_label_map("map",cls))
                cv2.putText(img, tags[-1], (xmin, ymin-10), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 1)
                # draw on image
                cv2.rectangle(img, (xmin, ymin), (xmax, ymax), (128, 0, 128), 4)

    # convert back to bgr and save image
    cv2.imwrite("output.png", cv2.cvtColor(img, cv2.COLOR_RGB2BGR))
    return tags



def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

if __name__=='__main__':

    app = flask.Flask(__name__)
    app.config["DEBUG"] = True
    app.secret_key = 'super secret key'
    app.config['SESSION_TYPE'] = 'filesystem'
    app_root = os.path.dirname(os.path.abspath(__file__))

    @app.route('/api/tag', methods=['GET'])
    def index():
        target = os.path.join(app_root, 'img/')
        if not os.path.isdir(target):
            os.mkdir(target)
        if request.method == 'GET':
            file = open('img.jpg','wb')
            urls = request.args.get('url')
            file.write(urllib.request.urlopen(urls).read())
            Tags = get_tag('img.jpg')
            jsonStr = json.dumps(Tags)
            return jsonStr
        return "fichier creer"

    @app.route('/', methods=['GET'])
    def home():
        Tags = get_tag("img.jpg")
        jsonStr = json.dumps(Tags)
        return jsonStr

    app.run(host="0.0.0.0")
