async function send_form() {

    console.log("sending form");
    let object = getFormData();
    console.log(object);
    
    const POST_LOGIN_URL="/api/auth/login"; 
    let context =   {
                        method: 'POST',
                        headers: { 'Content-Type': 'application/json' },
                        body: JSON.stringify(object)
                    };
    
    fetch(POST_LOGIN_URL, context)
        .then(async response => {
            if (!response.ok) {
                // get error message from body or default to response status
                const error = (data && data.message) || response.status;
                return Promise.reject(error);
            }

            if (response.status == 200) {
                document.location.href = '/';
            }

        })
        .catch(error => {
            console.log(error);
        });
}

async function logout() {
    const GET_LOGOUT_URL="/api/auth/logout"; 
    let context =   {
                        method: 'GET',
                    };
    
    fetch(GET_LOGOUT_URL, context)
        .then(async response => {
            if (!response.ok) {
                // get error message from body or default to response status
                const error = (data && data.message) || response.status;
                return Promise.reject(error);
            }

            if (response.status == 200) {
                document.location.href = '/login';
            }

        })
        .catch(error => {
            console.log(error);
        });
}

function getFormData() {

    const form = document.querySelector('#login_form');
    const data = Object.fromEntries(new FormData(form).entries());

    return {
        "username": data.username,
        "password": data.password,
    }
} 