let cardLists = [
    {
        family_name: "DC Comic",
        img_src: "http://www.superherobroadband.com/app/themes/superhero/assets/img/superhero.gif",
        name: "SUPERMAN",
        description: "The origin story of Superman relates that he was born Kal-El on the planet Krypton, before being rocketed to Earth as an infant by his scientist father Jor-El, moments before Krypton's destruction. Discovered and adopted by a farm couple from Kansas, the child is raised as Clark Kent and imbued with a strong moral compass. Early in his childhood, he displays various superhuman abilities, which, upon reaching maturity, he resolves to use for the benefit of humanity through a 'Superman' identity.",
        hp: 500,
        energy: 100,
        attack: 50,
        defense: 50,
        price: 200
    },
    {
        family_name: "DC Comic",
        img_src: "https://static.fnac-static.com/multimedia/Images/8F/8F/7D/66/6716815-1505-1540-1/tsp20171122191008/Lego-lgtob12b-lego-batman-movie-lampe-torche-batman.jpg",
        name: "BATMAN",
        description: "Bruce Wayne, alias Batman, est un héros de fiction appartenant à l'univers de DC Comics. Créé par le dessinateur Bob Kane et le scénariste Bill Finger, il apparaît pour la première fois dans le comic book Detective Comics no 27 (date de couverture : mai 1939 mais la date réelle de parution est le 30 mars 1939) sous le nom de The Bat-Man. Bien que ce soit le succès de Superman qui ait amené sa création, il se détache de ce modèle puisqu'il n'a aucun pouvoir surhumain. Batman n'est qu'un simple humain qui a décidé de lutter contre le crime après avoir vu ses parents se faire abattre par un voleur dans une ruelle de Gotham City, la ville où se déroulent la plupart de ses aventures. Malgré sa réputation de héros solitaire, il sait s'entourer d'alliés, comme Robin, son majordome Alfred Pennyworth ou encore le commissaire de police James Gordon. ",
        hp: 50,
        energy: 80,
        attack: 170,
        defense: 80,
        price: 100
    },
    {
        family_name: "Marvel",
        img_src: "https://static.hitek.fr/img/actualite/2017/06/27/i_deadpool-2.jpg",
        name: "DEAD POOL",
        description: "Le convoi d'Ajax est attaqué par Deadpool. Il commence par massacrer les gardes à l'intérieur d'une voiture, avant de s'en prendre au reste du convoi. Après une longue escarmouche, où il est contraint de n'utiliser que les douze balles qu'il lui reste, Deadpool capture Ajax (dont le véritable nom est Francis, ce que Deadpool ne cesse de lui rappeler). Après l'intervention de Colossus et Negasonic venus empêcher Deadpool de causer plus de dégâts et le rallier à la cause des X-Men, Ajax parvient à s'échapper en retirant le sabre de son épaule. Il apprend par la même occasion la véritable identité de Deadpool : Wade Wilson.",
        hp: 999999,
        energy: 100,
        attack: 15,
        defense: 15,
        price: 250
    },

]

function displaySellPage() {
    document.querySelector("#current_action_text").textContent = "SELL";
    document.querySelector("#curent_action_sub_text").textContent = "Sell cards to earn money!";
    // document.querySelector("#buy_sell_button").textContent = "Sell";
    get_cards(false, true);
    return show('sell_buy_page', 'main_page', 'play_page')
}


function displayBuyPage() {
    document.querySelector("#current_action_text").textContent = "BUY";
    document.querySelector("#curent_action_sub_text").textContent = "Buy new cards!";
    // document.querySelector("#buy_sell_button").textContent = "Buy";
    get_cards(true, false);

    return show('sell_buy_page','main_page','play_page');
}

function displayPlayPage() {
    document.querySelector("#current_action_text").textContent = "Play";
    document.querySelector("#curent_action_sub_text").textContent = "Cool";
    get_cards(false, false);

    return show('play_page','main_page','sell_buy_page');
}

function displayMainPage() {
    document.querySelector("#current_action_text").textContent = "Main Page";
    document.querySelector("#curent_action_sub_text").textContent = "";

    // Remove table data
    var elmtTable = document.getElementById('tableContent');
    var tableRows = elmtTable.getElementsByTagName('tr');
    var rowCount = tableRows.length;

    for (var x=rowCount-1; x>=0; x--) {
        elmtTable.removeChild(tableRows[x]);
    }

    return show('main_page','sell_buy_page','play_page');
}


function show(shown, hidden1, hidden2) {
    document.getElementById(shown).style.display='block';
    document.getElementById(hidden1).style.display='none';
    document.getElementById(hidden2).style.display='none';
    return false;
}

// parsedInfo[0] : username
// parsedInfo[1] : balance
// parsedInfo[2] : user id
function getUserInfoFromCookie() {
    var cookieName = "userInfo";
    var match = document.cookie.match(new RegExp('(^| )' + cookieName + '=([^;]+)'));
    var content = match[2]

    var splitContent = content.split("&");
    console.log(splitContent);
    var username = splitContent[0].split("=")[1];
    var id = splitContent[1].split("=")[1];

    var parsedInfo = [username, id];
    return parsedInfo;
}

async function get_user_with_token() {
    var parsedInfo = getUserInfoFromCookie();
    var username = parsedInfo[0];

    const GET_USER_WITH_TOKEN = "/api/user/" + username;

    let context = {
        method: 'GET'
    };

    fetch(GET_USER_WITH_TOKEN, context)
        .then(async response => {
            if (!response.ok) {
                //get error message from body or default to response status
                // const error = (data && data.message) || response.status;
                return Promise.reject(error);
            }
            return response.json();
        })
        .then(data => {
            console.log("GET TOKEN DATA", data);
            setUserInfo(data.username, data.balance);
            return data;
        })
        .catch(error => {
            console.log(error);
        });

}

async function setUserInfo(username, balance) {
    document.querySelector("#username_id").textContent = username;
    document.querySelector("#balance_value").textContent = balance;
}

async function get_card(elem) {
    var card_id = elem.id;
    const GET_CARD_URL = "/api/card/" + card_id;

    let context = {
        method: 'GET'
    };

    fetch(GET_CARD_URL, context)
        .then(async response => {
            if (!response.ok) {
                //get error message from body or default to response status
                // const error = (data && data.message) || response.status;
                return Promise.reject(error);
            }
            return response.json();
        })
        .then(data => {
            console.log(data);
            displayFullCard(data);
        })
        .catch(error => {
            console.log(error);
        });

}

async function get_cards(buy, sell) {

    console.log("Getting own cards");

    data = get_user_with_token();
    user_id = data.id;

    // Buy: cards the user can buy
    // Sell: cards the user has and can sell
    // Else: all cards the user owns

    var GET_CARDS_URL = "";

    if (buy) {
        GET_CARDS_URL = "/api/card/user/buy/";
    } else if (sell) {
        GET_CARDS_URL = "/api/card/user/sell/";
    } else {
        GET_CARDS_URL = "/api/card/user/" + user_id;
    }

    let context = {
        method: 'GET'
    };

    fetch(GET_CARDS_URL, context)
        .then(async response => {
            if (!response.ok) {
                //get error message from body or default to response status
                // const error = (data && data.message) || response.status;
                return Promise.reject(error);
            }
            return response.json();
        })
        .then(data => {
            console.log(data);
            displayCards(data, sell);
        })
        .catch(error => {
            console.log(error);
        });
}

function displayCards(cards, isSelling) {
    console.log(cards);
    if (!cards) return

    for (var card of cards) {
        console.log(card);
        type  =  card.type;
        let template = document.querySelector("#row");
        let clone = document.importNode(template.content, true);

        newContent = clone.firstElementChild.innerHTML
            // .replace(/{{onclick_function}}/g, "test()")
            .replace(/{{name}}/g, type.name)
            .replace(/{{img_src}}/g, type.imgUrl)
            .replace(/{{description}}/g, type.description)
            .replace(/{{family}}/g, type.family)
            .replace(/{{health}}/g, type.health)
            .replace(/{{energy}}/g, type.energy)
            .replace(/{{defense}}/g, type.defense)
            .replace(/{{attack}}/g, type.attack)
            .replace(/{{price}}/g, type.price)
            .replace(/{{card_id}}/g, card.id);



        clone.firstElementChild.innerHTML = newContent;
        clone.firstElementChild.firstElementChild.setAttribute('onclick', 'get_card(this)');
        clone.firstElementChild.id = card.id;
        // clone.onclick = displayCards;

        buy_sell_buttons = document.querySelectorAll(".buy_sell_button");

        for (button of buy_sell_buttons) {
            if (isSelling){
                button.setAttribute('onclick', 'sellCard(this)');
            } else {
                button.setAttribute('onclick', 'buyCard(this)');
            }
            console.log(button);
        }


        let cardContainer = document.querySelector("#tableContent");

        cardContainer.appendChild(clone);
    }
}

function displayFullCard(data) {

    card = data
    if (!card) return

    let cardContainer = document.querySelector("#card");
    // cardContainer.innerHTML = "";

    type  =  card.type;
    let template = document.querySelector("#full_card");
    let clone = document.importNode(template.content, true);

    newContent = clone.firstElementChild.innerHTML
        .replace(/{{name}}/g, type.name)
        .replace(/{{img_url}}/g, type.imgUrl)
        .replace(/{{description}}/g, type.description)
        .replace(/{{family}}/g, type.family)
        .replace(/{{health}}/g, type.health)
        .replace(/{{energy}}/g, type.energy)
        .replace(/{{defense}}/g, type.defense)
        .replace(/{{attack}}/g, type.attack)
        .replace(/{{price}}/g, type.price);
    clone.firstElementChild.innerHTML = newContent;

    cardContainer.innerHTML = "";
    cardContainer.appendChild(clone);
}


async function logout() {
    const GET_LOGOUT_URL="/api/auth/logout"; 
    let context =   {
                        method: 'GET',
                    };
    
    fetch(GET_LOGOUT_URL, context)
        .then(async response => {
            if (!response.ok) {
                // get error message from body or default to response status
                // const error = (data && data.message) || response.status;
                return Promise.reject(error);
            }

            if (response.status == 200) {
                window.location.reload();
            }

        })
        .catch(error => {
            console.log(error);
        });
}


async function sellCard(elem) {
    console.log("sell", elem);
    const cardID = elem.children[0].id;
    const SELL_CARD_URL = "/api/card/sell/";
    let context =   {
                        method: 'POST',
                    };
    
    fetch(SELL_CARD_URL + cardID, context)
        .then(async response => {
            if (!response.ok) {
                // get error message from body or default to response status
                // const error = (data && data.message) || response.status;
                return Promise.reject(error);
            }

            if (response.status == 200) {
                let tr = document.getElementById(cardID);
                // console.log(tr)
                tr.remove();
            }

        })
        .catch(error => {
            console.log(error);
        });
}


async function buyCard(elem) {
    //console.log("sold");
    const SELL_CARD_URL = "/api/transaction/buy/";
    id = getUserInfoFromCookie();
    let context =   {
                        method: 'POST',
                        body: JSON.stringify({ UserId: id[2], CardId: elem })
                    };
    
    fetch(SELL_CARD_URL+elem, context)
        .then(async response => {
            if (!response.ok) {
                // get error message from body or default to response status
                // const error = (data && data.message) || response.status;
                return Promise.reject(error);
            }

            if (response.status == 200) {
            }

        })
        .catch(error => {
            console.log(error);
        });
}

async function islogin() {
    const POST_VERIFYTOKEN_URL="/api/auth/is-logged-in"; 

    let context =   {
        method: 'POST',
        credentials: 'include'
    };
    
    fetch(POST_VERIFYTOKEN_URL, context)
        .then(async response => {
            if (!response.ok) {
                // get error message from body or default to response status
                // const error = (data && data.message) || response.status;
                //set page to login page
                console.log("No user logged in, back to login page");
                window.location.replace("/login");
                return Promise.reject(error);
            }

            if (response.status == 200) {
                console.log("user is logged in");
                get_user_with_token();
            }

        })
        .catch(error => {
            console.log(error);
        });
}

window.onload = islogin;

