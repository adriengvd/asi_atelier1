# Atelier III

- Launch pgAdmin: `docker-compose up -d pgadmin4`

- Access pgAdmin at `localhost:8004`, login with `root@root.com` and `root`

- Create the 3 necessary databases: `cards`, `user` and `transactions`

- Launch all other docker containers: `docker-compose up -d`

- Launch all 4 springboot microservices
