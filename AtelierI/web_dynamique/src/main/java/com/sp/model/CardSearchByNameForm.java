package com.sp.model;

public class CardSearchByNameForm {
    private String name;

    public CardSearchByNameForm() {
        this.name = "";
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
