package com.sp.model;

public class Card  {

	private String description;
	private String family;
	private String name;
	private String imgUrl;
	private String affinity;
	private float hp;
	private float energy;
	private float attack;
	private float defense;

	public Card() {
		this.description = "";
		this.family = "";
		this.name = "";
		this.imgUrl = "";
		this.affinity = "";
		this.hp = 0;
		this.energy = 0;
		this.attack = 0;
		this.defense = 0;
	}

	public Card(String name, String description, String family, String imgUrl, String affinity, float hp, float energy, float attack, float defense) {
		this.description = description;
		this.family = family;
		this.name = name;
		this.imgUrl = imgUrl;
		this.affinity = affinity;
		this.hp = hp;
		this.energy = energy;
		this.attack = attack;
		this.defense = defense;
	}
	
	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}


	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFamily() {
		return family;
	}

	public void setFamily(String family) {
		this.family = family;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;	
	}

	public String getAffinity() {
		return affinity;
	}

	public void setAffinity(String affinity) {
		this.affinity = affinity;	
	}

	public float getHp() {
		return hp;
	}
	
	public void setHp(float hp) {
		this.hp = hp;	
	}

	public float getEnergy() {
		return energy;
	}

	public void setEnergy(float energy) {
		this.energy = energy;	
	}

	public float getAttack() {
		return attack;
	}

	public void setAttack(float Attack) {
		this.attack = Attack;	
	}

	public float getDefense() {
		return defense;
	}

	public void setDefense(float defense) {
		this.defense = defense;	
	}

}
