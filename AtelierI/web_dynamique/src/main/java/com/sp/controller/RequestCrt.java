package com.sp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.sp.model.Card;
import com.sp.model.CardFormDTO;
import com.sp.model.CardSearchByNameForm;
import com.sp.service.CardService;

@Controller // AND NOT @RestController
public class RequestCrt {
	
	@Autowired
	CardService cardService;
	
	@RequestMapping(value = { "/", "/index" }, method = RequestMethod.GET)
	public String index(Model model) {
		return "index";
	}
	
	@RequestMapping(value = {"/cardView"}, method = RequestMethod.GET)
	public String view(Model model) {
		CardSearchByNameForm searchForm = new CardSearchByNameForm();
		boolean isSearch = false;

		model.addAttribute("searchForm", searchForm);
		model.addAttribute("isSearch", isSearch);

		return "CardView";
	}

	@RequestMapping(value = {"/cardView"}, method = RequestMethod.POST)
	public String view(Model model, @ModelAttribute("searchForm") CardSearchByNameForm searchForm) {
		boolean isSearch = true;
		model.addAttribute("isSearch", isSearch);

		Card search = cardService.getCardByName(searchForm.getName());
		if (search != null) {
			model.addAttribute("myCard", search);
		}
		return "CardView";
	}

	
	@RequestMapping(value = {"/addCard"}, method = RequestMethod.GET)
	public String addCard(Model model) {
		CardFormDTO CardForm = new CardFormDTO();
		boolean sentForm = false;
		model.addAttribute("CardForm", CardForm);
		model.addAttribute("sentForm", sentForm);
		return "CardForm";
	}
	
	
	@RequestMapping(value = {"/addCard"}, method = RequestMethod.POST)
	public String addCard(Model model, @ModelAttribute("CardForm") CardFormDTO CardForm) {
		boolean isRequestSuccessful;
		boolean sentForm = true;
		try {
			this.cardService.addCard(CardForm);
			isRequestSuccessful = true;
		} catch (Error e) {
			isRequestSuccessful = false;
		}

		model.addAttribute("success", isRequestSuccessful);
		model.addAttribute("sentForm", sentForm);
		return "CardForm";
	}

}
