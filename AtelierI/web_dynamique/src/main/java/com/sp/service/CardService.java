package com.sp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import com.sp.model.Card;
import com.sp.model.CardFormDTO;
import com.sp.repository.CardRepository;

@Service
public class CardService {
    
    @Autowired
	public CardRepository cardRepository;

    public List<Card> getAllCards() {
        return this.cardRepository.getCardList();
    }

    public Card getCardByName(String name) {
        return this.cardRepository.getCardByName(name);
    }

    public Card addCard(CardFormDTO card) {
        return this.cardRepository.addCard(card.getName(), card.getDescription(), card.getImgUrl(), card.getFamily(), card.getAffinity(), card.getHp(), card.getAttack(), card.getEnergy(), card.getDefense());
    }
}
