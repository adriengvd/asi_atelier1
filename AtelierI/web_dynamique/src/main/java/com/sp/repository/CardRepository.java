package com.sp.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.stereotype.Service;

import com.sp.model.Card;

@Service
public class CardRepository {
	private List<Card> myCardList;
	private Random randomGenerator;
	
	public CardRepository() {
		myCardList = new ArrayList<>();
		randomGenerator = new Random();
	}

	public List<Card> getCardList() {
		return this.myCardList;
	}

	public Card getCardByName(String name){
		for (Card CardBean : myCardList) {
			if (CardBean.getName().equals(name)){
				return CardBean;
			}
		}
		return null;
	}

	public Card getRandomCard(){
		if (this.myCardList.size() != 0) {
			int index = randomGenerator.nextInt(this.myCardList.size());
			return this.myCardList.get(index);
		}

		return null;
	}

	public Card addCard(String name, String description, String imgUrl, String family, String affinity, float hp, float attack, float energy, float defense) {
		Card c = new Card(name, description, family, imgUrl, affinity, hp, energy, attack, defense);
		this.myCardList.add(c);
		return c;
	}
}
