window.onload = () => {
    let message = document.querySelector(".icon")
    
    if (message != null) {
        message.addEventListener('click', () => {
            let div = message.closest('.message');
            div.style.visibility = "hidden"
            let container = document.querySelector("#message-container")
            container.style.height = 0
        })
    }

}
