let cardlist = [
        {
            smallImgUrl:"https://imgc.allpostersimages.com/img/print/affiches/marvel-super-hero-squad-iron-man-standing_a-G-9448041-4985690.jpg",
            family_name:"Jose",
            // smallImgUrl:"https://imgc.allpostersimages.com/img/print/affiches/marvel-super-hero-squad-iron-man-standing_a-G-9448041-4985690.jpg",
            date:"14h",
            comment:"3 comments",
            like: "17 likes",
            button: "Buy"
        },
        {
            smallImgUrl:"https://media.giphy.com/media/l4q8hciiYNT5RGi4w/giphy.gif",
            family_name:"John",
            // image_src:"https://media.giphy.com/media/l4q8hciiYNT5RGi4w/giphy.gif",
            date:"1h",
            comment:"345 comments",
            like: "1000 likes",
            button: "Read"
        },
        {
            smallImgUrl:"http://www.superherobroadband.com/app/themes/superhero/assets/img/superhero.gif",
            family_name:"Elliot",
            // image_src:"http://www.superherobroadband.com/app/themes/superhero/assets/img/superhero.gif",
            date:"142h",
            comment:"0 comment",
            like: "1 likes",
            button: "Read"
        }

    ];
let template = document.querySelector("#selectedCard");

for(const card of cardlist){
    let clone = document.importNode(template.content, true);

    newContent= clone.firstElementChild.innerHTML
    // .replace(/{{smallImgUrl}}/g, card.smallImgUrl)
        .replace(/{{family_name}}/g, card.family_name)
        .replace(/{{smallImgUrl}}/g, card.smallImgUrl)
        .replace(/{{name}}/g, card.name)
        .replace(/{{description}}/g, card.description)
        .replace(/{{hp}}/g, card.hp)
        .replace(/{{energy}}/g, card.energy)
        .replace(/{{attack}}/g, card.attack)
        .replace(/{{defense}}/g, card.defense);
    clone.firstElementChild.innerHTML= newContent;

    let cardContainer= document.querySelector("#gridContainer");
    cardContainer.appendChild(clone);
}





