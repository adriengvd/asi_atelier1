async function send_form() {

    console.log("sending form");
    let object = getFormData();
    console.log(object);
    
    const POST_CARD_URL="https://asi2-backend-market.herokuapp.com/card"; 
    let context =   {
                        method: 'POST',
                        headers: { 'Content-Type': 'application/json' },
                        body: JSON.stringify(object)
                    };
    
    fetch(POST_CARD_URL,context)
        .then(async response => {
            if (!response.ok) {
                // get error message from body or default to response status
                const error = (data && data.message) || response.status;
                return Promise.reject(error);
            }
        })
        .catch(error => {
            console.log(error);
        });
}

function getFormData() {

    const form = document.querySelector('#add_card_form');
    const data = Object.fromEntries(new FormData(form).entries());

    return {
        "name": data.name,
        "description": data.description,
        "family": data.family,
        "affinity": data.affinity,
        "imgUrl": data.imageUrl,
        "smallImgUrl": data.imageUrl,
        "energy": data.energy,
        "hp": data.hp,
        "defence": data.defence,
        "attack": data.attack,
        "price": 0,
        "userId": null,
        "storeId": null
    }
} 