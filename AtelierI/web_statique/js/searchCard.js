
async function get_card() {

    console.log("Getting cards");

    const GET_CARD_URL = "https://asi2-backend-market.herokuapp.com/cards";
    let context = {
        method: 'GET'
    };

    fetch(GET_CARD_URL, context)
        .then(async response => {
            if (!response.ok) {
                // get error message from body or default to response status
                const error = (data && data.message) || response.status;
                return Promise.reject(error);
            }
            return response.json();
        })
        .then(data => {
            console.log(data);
            console.log("filtre");
            console.log(FilterByName(data, getFormData()));
            LoadCard(FilterByName(data, getFormData()));
        })
        .catch(error => {
            console.log(error);
        });
}

function FilterByName (array, name) {
    const newArray = array.filter(a => a.name === name);
    return newArray[0]
}

function getFormData() {
    const form = document.querySelector('#mySearch');
    const data = Object.fromEntries(new FormData(form).entries());

    return data.search
}

function LoadCard(card) {
    if (!card) return
    let template = document.querySelector("#selectedCard");
    let clone = document.importNode(template.content, true);
    
    newContent = clone.firstElementChild.innerHTML
        .replace(/{{family_src}}/g, card.smallImgUrl)
        .replace(/{{family_name}}/g, card.family)
        .replace(/{{img_src}}/g, card.imgUrl)
        .replace(/{{name}}/g, card.name)
        .replace(/{{description}}/g, card.description)
        .replace(/{{hp}}/g, card.hp)
        .replace(/{{energy}}/g, card.energy)
        .replace(/{{attack}}/g, card.attack)
        .replace(/{{defense}}/g, card.defence);
    clone.firstElementChild.innerHTML = newContent;
    
    let cardContainer = document.querySelector("#cardContainer");
    cardContainer.appendChild(clone);
    
}






